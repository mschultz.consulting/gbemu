# GBEMU
Useful links:

http://www.pastraiser.com/cpu/gameboy/gameboy_opcodes.html (Shows the opcodes specific for the gameboy)

http://gbdev.gg8.se/wiki/articles/CPU_Comparision_with_Z80 (Shows logic with the new opcodes)

http://stackoverflow.com/questions/8868396/gbz80-what-constitutes-a-half-carry (Describes the half-carry flag)

http://clrhome.org/table/# (Describes the Z80 instructions)

http://marc.rawer.de/Gameboy/Docs/GBCPUman.pdf (Describes the entire system)

Possible bug with jump?
