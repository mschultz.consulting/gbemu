#include "includes.h"

uint8_t SoundCard::SoundMode::GetSweepRegister(){
    return (sweepTime <<4) + (sweepDecrease*0b1000) + sweepShift;
}

void SoundCard::SoundMode::SetSweepRegister(uint8_t value)
{
    sweepTime =(( 0b01110000&value)>>4);
    sweepDecrease = ((value&0b1000) != 0);
    sweepShift = (value&0b111);
}

uint8_t SoundCard::SoundMode::GetLengthWavePatternDuty()
{
    return (((uint8_t)waveDuty)<<6) + soundLength;
}

void SoundCard::SoundMode::SetLengthWavePatternDuty(uint8_t value)
{
    waveDuty = (WaveDuty)(value>>6);
    soundLength = value&0b111111;
}


SoundCard::SoundMode::SoundMode()
    : sweepTime(0), sweepDecrease(false), sweepShift(0),waveDuty(WaveDuty::_50), soundLength(0)
{
}
