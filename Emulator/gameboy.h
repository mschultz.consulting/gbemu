#ifndef GAMEBOY_H
#define GAMEBOY_H
#include "includes.h"
#include <chrono>
typedef std::chrono::high_resolution_clock Clock;

class GameBoy
{
public:
    GameBoy(uint8_t *bios, uint64_t size, uint8_t *cartData, uint64_t cartSize);
    void Boot(uint8_t* BIOS, uint16_t bios_size);
    Bus* bus;
    void Tick();
private:
    Clock::time_point oldClock = Clock::now();

    unsigned long long oldCycles = 0;
    void FrameLimiter(float limitMultiplier);

};

#endif // GAMEBOY_H
