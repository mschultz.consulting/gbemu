#include "includes.h"
#include "bitset"
#include "allegro5/allegro.h"

static uint8_t* ReadBinary(char* fileName, long* size)
{
    using namespace std;
    char* block;
    std::ifstream file (fileName, ios::binary|ios::in|ios::ate);
    *size = file.tellg();
    block = new char[*size];
    file.seekg(0, std::ios::beg);
    file.read(block, *size);
    file.close();
    return (uint8_t*)block;
}

/*class pixel{
public:
    uint8_t x,y;
    uint8_t val;
};*/



int main(int argc, char *argv[])
{
//    GBEMUTest tst = GBEMUTest();
  //  tst.ExecuteTest();
    //return 0;
    using namespace std;
    if (argc != 3)
    {
        cout<< "Usage: <Bios file> <.gb file>" <<endl;
        return 0;
    }
    long bios_len = 0;
    long game_len = 0;
    uint8_t* bios = ReadBinary(argv[1], &bios_len);
    uint8_t* game = ReadBinary(argv[2], &game_len);
    GameBoy *gb = new GameBoy(bios, bios_len,game, game_len);
    gb->bus->Cpu->Debug = true;
    while(true){

    gb->Tick();

    }


    return 0;
}

