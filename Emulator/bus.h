#ifndef BUS_H
#define BUS_H
#include "includes.h"


class Bus
{
public:
    Bus();
    Cart* BIOS;
    Input* Buttons;
    Cart* CartData;
    CPU* Cpu;
    GPU* Gpu;
    MemoryMap* Map;
    SoundCard* Sound;

    ~Bus();
    Bus(const Bus&);
    Bus& operator=(const Bus&);

};

#endif // BUS_H
