#include "includes.h"
std::string currentDateTime(){
    time_t now = time(0);
    struct tm tstruct;
    char buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
    return buf;
}
bool BitEnabled(uint8_t value, Bit bit)
{
    return ((value & bit) > 0);
}

void checkOps(CPU* cpu)
{
    char q = -11;
    for (int i = 0; i < 255; i++)
    {
        //std::cout<<std::hex<<i<<": "<<cpu->DetermineOpcode(i)->name()<<std::endl;
        if ( cpu->DetermineOpcode(i)->name()== "NOP")
        {
            q++;
            //std::cout<<std::hex<<i<<std::endl;
        }
    }
    std::cout<<std::dec<<(int)q<<" are missing"<<std::endl;
}
