#include "includes.h"


CPU::CPU(Bus* busv)
    : bus(busv)
    , opcodeMap()
    ,Debug(0)
    ,reg_A (0)
    ,reg_B (0)
    ,reg_C (0)
    ,reg_D (0)
    ,reg_E (0)
    ,reg_F (0)
    ,reg_H (0)
    ,reg_L (0)

    ,reg_PC (0)
    ,reg_SP (0)

    ,dividerRegister (0)
    ,timerRegister (0)
    ,timerModulo (0)
    ,timerEnabled ( true)
    ,clockspeed (CPU::TimerClockspeed::_4096Khz)
    ,InterruptFlagRegister (0)
    , NOPCode(new NOP())
    , HaltCode(new HALT())
    , Cycles(0)
    , InterruptMasterEnable(false)
    , InterruptEnableFlag(0x0)
{
    opcodeMap.push_back(NOPCode);
    opcodeMap.push_back(new MOV());
    opcodeMap.push_back(new MVI());
    opcodeMap.push_back(new LXI());
    opcodeMap.push_back(new LOGIC());
    opcodeMap.push_back(new JR());
    opcodeMap.push_back(new LDOffset());
    opcodeMap.push_back(new BitManipOP());
    opcodeMap.push_back(new LDIOConst());
    opcodeMap.push_back(new LDIOC());
    opcodeMap.push_back(new OffsetOne());
    opcodeMap.push_back(new LDAX());
    opcodeMap.push_back(new CALL());
    opcodeMap.push_back(new CheckCALL());
    opcodeMap.push_back(new PUSH());
    opcodeMap.push_back(new RotateA());
    opcodeMap.push_back(new RET());
    opcodeMap.push_back(new CheckRET());
    opcodeMap.push_back(new POP());
    opcodeMap.push_back(new ADDSUB());
    opcodeMap.push_back(new Offset16());
    opcodeMap.push_back(new RST);
    opcodeMap.push_back(new LogicImmediate());
    opcodeMap.push_back(new LDXI());
    opcodeMap.push_back(new JRI());
    opcodeMap.push_back(new JP());
    opcodeMap.push_back(new ADDHL());
    opcodeMap.push_back(new ADDSUBIMMEDIATE());
    opcodeMap.push_back(new CFOp());
    opcodeMap.push_back(new CheckJP());
    opcodeMap.push_back(new EIDI());
    opcodeMap.push_back(new RETI());
    opcodeMap.push_back(new LDMSP());
    opcodeMap.push_back(new CPL());
    opcodeMap.push_back(new ADDSP());
    opcodeMap.push_back(new JPHL());
    opcodeMap.push_back(new LDSPHL());
    opcodeMap.push_back(new LDHLSPI());
    opcodeMap.push_back(new DAA());
}
CPU::CPU(const CPU & copy)
    : bus ( copy.bus)
    ,opcodeMap(copy.opcodeMap)

    , Debug(copy.Debug)
    ,reg_A ( copy.reg_A)
    ,reg_B ( copy.reg_B)
    ,reg_C ( copy.reg_C)
    ,reg_D ( copy.reg_D)
    ,reg_E ( copy.reg_E)
    ,reg_F ( copy.reg_F)
    ,reg_H ( copy.reg_H)
    ,reg_L ( copy.reg_L)

    ,reg_PC ( copy.reg_PC)
    ,reg_SP ( copy.reg_SP)

    ,dividerRegister ( copy.dividerRegister)
    ,timerRegister ( copy.timerRegister)
    ,timerModulo ( copy.timerModulo)
    ,timerEnabled ( copy.timerEnabled)
    ,clockspeed ( copy.clockspeed)
    ,InterruptFlagRegister ( copy.InterruptFlagRegister)
    ,NOPCode ( copy.NOPCode)
    , Cycles(copy.Cycles)
    , InterruptMasterEnable(copy.InterruptMasterEnable)
{
}

CPU& CPU::operator =(const CPU& copy)
{
    this->opcodeMap = copy.opcodeMap;
    this->bus = copy.bus;
    this->reg_A = copy.reg_A;
    this->reg_B = copy.reg_B;
    this->reg_C = copy.reg_C;
    this->reg_D = copy.reg_D;
    this->reg_E = copy.reg_E;
    this->reg_F = copy.reg_F;
    this->reg_H = copy.reg_H;
    this->reg_L = copy.reg_L;
    this->Debug = copy.Debug;

    this->reg_PC = copy.reg_PC;
    this->reg_SP = copy.reg_SP;

    this->NOPCode = copy.NOPCode;

    this->dividerRegister = copy.dividerRegister;
    this->timerRegister = copy.timerRegister;
    this->timerModulo = copy.timerModulo;
    this->timerEnabled = copy.timerEnabled;
    this->clockspeed = copy.clockspeed;
    this->InterruptFlagRegister = copy.InterruptFlagRegister;
    this->InterruptMasterEnable = copy.InterruptMasterEnable;
    return *this;
}

CPU::~CPU()
{
    for(auto val : opcodeMap)
    {
        delete val;
    }
    //delete NOPCode;
}

void CPU::Set8BitRegister(Register8 reg, uint8_t val)
{
    switch (reg)
    {
    case Register8::A:
        reg_A = val;
        break;
    case Register8::B:
        reg_B = val;
        break;
    case Register8::C:
        reg_C = val;
        break;
    case Register8::D:
        reg_D = val;
        break;
    case Register8::E:
        reg_E = val;
        break;
    case Register8::H:
        reg_H = val;
        break;
    case Register8::L:
        reg_L = val;
        break;
    case Register8::M:
        DMA(Get16bitRegisterValue(Register16::HL), val);
        break;
    case Register8::F:
        reg_F = val;
        break;

    }
}

uint8_t CPU::Get8BitRegister(Register8 reg)
{
    switch (reg)
    {
    case Register8::A:
        return reg_A;
    case Register8::B:
        return reg_B;
    case Register8::C:
        return reg_C;
    case Register8::D:
        return reg_D;
    case Register8::E:
        return reg_E;
    case Register8::H:
        return reg_H;
    case Register8::L:
        return reg_L;
    case Register8::M:
        return DMA(Get16bitRegisterValue(Register16::HL));
    case Register8::F:
        return reg_F;
    }
    return 0x0;
}

void CPU::DMA(uint16_t address, uint8_t val)
{
    Cycles += 4;
    bus->Map->SetMemory(address, val);
}

uint8_t CPU::DMA(uint16_t address)
{
    Cycles += 4;
    return bus->Map->GetMemory(address);
}

std::string CPU::Get8BitRegisterName(Register8 reg)
{
    switch (reg)
    {
    case Register8::A:
        return "reg_A";
    case Register8::B:
        return "reg_B";
    case Register8::C:
        return "reg_C";
    case Register8::D:
        return "reg_D";
    case Register8::E:
        return "reg_E";
    case Register8::H:
        return "reg_H";
    case Register8::L:
        return "reg_L";
    case Register8::M:
        return "memory";
    case Register8::F:
        return "flags";
    }
    return 0x0;
}

uint16_t CPU::Get16bitRegisterValue(Register16 reg)
{
    uint16_t val =0x0;
    if (reg == Register16::BC)
        val = ((uint16_t)this->reg_B)<<8 | reg_C;
    else if (reg == Register16::DE)
        val = ((uint16_t)this->reg_D)<<8|reg_E;
    else if (reg == Register16::HL)
        val = ((uint16_t)this->reg_H)<<8|reg_L;
    else if (reg == Register16::PC)
        val = this->reg_PC;
    else if (reg == Register16::SP)
        val = this->reg_SP;
    else if (reg == Register16::AF)
        val = ((uint16_t)this->reg_A)<<8|reg_F;
    return val;
}

void CPU::AddTo16BitRegister(Register16 reg, signed char val){
    uint16_t val16 = Get16bitRegisterValue(reg);
    val16 += val;
    Set16BitRegister(reg, val16);
}

void CPU::Tick(){
    ProcessInterrupts();
    uint8_t ins = DMA(reg_PC++);
    OPCode* opcode = DetermineOpcode(ins);
    OPCodeParameters params(this);
    if (opcode != NULL)
    {
        //Lower byte is pulled off RAM first
        if (opcode->numberAdditionalBytes() >= 1)
            params.LowByte =  DMA(reg_PC++);
        if (opcode->numberAdditionalBytes() >= 2)
            params.HighByte = DMA(reg_PC++);
        params.OPCode = ins;
    }
    else
    {
        opcode = NOPCode;
    }

    if ( !bus->Map->readFromBios)
    {
        std::cout<<std::hex<<(int)ins<< " ";
        if (opcode->numberAdditionalBytes() >= 1)
            std::cout<<":B1:"<<std::hex<<(int)params.LowByte;
        if (opcode->numberAdditionalBytes() >= 2)
            std::cout<<":B2:"<<std::hex<<(int)params.LowByte;
        std::cout<<opcode->name()<<std::endl;
        for (int i = 0; i < 10; i++)
        {
            int offset = i - 4;
            std::cout<< " "<<std::hex<<(int)DMA(reg_PC + offset);
        }
        std::cout<<std::endl;
        PrintReg();
    }
    opcode->Execute(params);
 }

void CPU::Set16BitRegister(Register16 reg, uint16_t val)
{
    if (reg == Register16::BC){
        reg_B = (uint8_t)(val>>8);
        reg_C = (uint8_t)val;
    }
    if (reg == Register16::DE){
        reg_D = (uint8_t)(val>>8);
        reg_E = (uint8_t)val;
    }
    if (reg == Register16::HL){
        reg_H = (uint8_t)(val>>8);
        reg_L = (uint8_t)val;
    }
    if (reg == Register16::PC){
        Cycles += 4;
        reg_PC = val;
    }
    if (reg == Register16::SP){
        reg_SP = val;
    }
    if (reg == Register16::AF){
        reg_A = (uint8_t)(val>>8);
        reg_F = (uint8_t)(val&0b11110000);
    }
}

OPCode* CPU::DetermineOpcode(uint8_t instruction){
    OPCode* tmp = NOPCode;
    int test =0;
    if (instruction == HaltCode->checkOpcode())
    {
        std::cout<<"Found HALT-Code"<<std::endl;
        return HaltCode;
    }
    for(auto val : opcodeMap)
    {
        if (((instruction & val->formatOpcode())^val->checkOpcode()) ==0)
        {
            test++;
            tmp= val;
        }
    }
    if (test !=1)
    {
        Debug = true;
        std::cout<<"Found "<<test<<" for "<<std::hex<<(int)instruction<<std::dec<<std::endl;
    }
    return tmp;
}

void CPU::ProcessInterrupts()
{
    if (!InterruptMasterEnable)
        return;
    for (int i = 0; i < 5; i++)
    {
        if (((InterruptFlagRegister>>i)&0x1) == 0x1)
        {
            if (((InterruptEnableFlag>>i)&0x1) == 0x1)
            {
                InterruptMasterEnable = false;
                InterruptFlagRegister = InterruptFlagRegister & ((1<<i)^0xFF);
                OPCodeParameters params(this);
                params.OPCode = 0xCD;
                params.HighByte = 0x0;
                params.LowByte = ((i * 8) + 0x40);
                CALL call;
                call.Execute(params);
                return;
            }
        }
    }
}

void CPU::AcceptInterrupt(Interrupt interrupt)
{
    InterruptFlagRegister |= (1<<GetInterruptBit(interrupt));
}
uint8_t CPU::GetInterruptBit(Interrupt i)
{
    uint8_t ret = i;
    ret-=0x40;
    ret/=0x8;
    return ret;
}

void CPU::PrintReg(){
    using namespace std;
    cout<<hex<<"A: "<<(int)reg_A<< "|"
       <<"F: "<<(int)reg_F<<endl
      <<"B: "<<(int)reg_B<<"|"
     <<"C: "<<(int)reg_C<< endl
    <<"D: "<<(int)reg_D<<"|"
    <<"E: "<<(int)reg_E<< endl
    <<"H: "<<(int)reg_H<< "|"
    <<"L: "<<(int)reg_L<<endl
    <<"SP: "<<(int)reg_SP<< "|"
    <<"PC: "<<(int)reg_PC<<endl<<dec
    <<"Z:"<<(int)GetFlag(CPU::Flag::F_Z)<<"|N:"<<(int)GetFlag(CPU::Flag::F_N)
    <<"|H:"<<(int)GetFlag(CPU::Flag::F_H)<<"|C:"<<(int)GetFlag(CPU::Flag::F_C)<<std::endl<<std::endl;
}

