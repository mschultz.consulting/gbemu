#ifndef MBC3_H
#define MBC3_H
#include "../includes.h"

class MBC3 : public MemoryMap
{
public:
    MBC3(Bus* val);
    void SetROMBank0(uint16_t address, uint8_t value);
    uint8_t GetSwitchableROMBank(uint16_t address);
protected:
    uint8_t romSelector;
   const uint16_t ROM_BANK_0_SWAP_MIN_ADDRESS = 0x2000;
};

#endif // MBC3_H
