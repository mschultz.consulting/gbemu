#include "../includes.h"

MBC3::MBC3(Bus* val) : MemoryMap(val, true), romSelector(1)
{

}


void MBC3::SetROMBank0(uint16_t address, uint8_t value)
{
    if (address >= ROM_BANK_0_SWAP_MIN_ADDRESS && address < ROM_BANK_0_MAX_ADDR)
    {
        std::cout<<"Chaning ROM selector to"<<std::dec<<value<<std::endl;
        if (value == 0)
            value = 1;
        romSelector = value;
    }

}

uint8_t MBC3::GetSwitchableROMBank(uint16_t address)
{
    uint16_t offset = address - ROM_BANK_0_MAX_ADDR;
    uint16_t baseAddress =  ((SWITCHABLE_ROM_BANK_MAX_ADDR - ROM_BANK_0_MAX_ADDR) * (romSelector));
    return MemoryMap::GetSwitchableROMBank(baseAddress+offset);
}
