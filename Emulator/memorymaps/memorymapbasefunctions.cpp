#include "../includes.h"
//This is the baseline functions for the memory map.
//These should mostly be overwritten by the child objects.
void MemoryMap::SetROMBank0(uint16_t address, uint8_t value)
{
    //std::cout<<std::string("Attempt to write ROMBank0 A:") +std::to_string(address) + std::string(" V: ") + std::to_string(value)<<std::endl;
}
void MemoryMap::SetSwitchableROMBank(uint16_t address, uint8_t value)
{
    //std::cout<<std::string("Attempt to write SwitchableROMBank0 A:") +std::to_string(address) + std::string(" V: ") + std::to_string(value)<<std::endl;
}
void MemoryMap::SetVRAM(uint16_t address, uint8_t value)
{
    //Log(std::string("Attempt to write VRAM A:") +std::to_string(address) + std::string(" V: ") + std::to_string(value) );
    VRAM[address - SWITCHABLE_ROM_BANK_MAX_ADDR] = value;
}
void MemoryMap::SetSwitchableRAMBank(uint16_t address, uint8_t value)
{
    //Log(std::string("Attempt to write SwitchableRAMBank A:") +std::to_string(address) + std::string(" V: ") + std::to_string(value) );
    SwitchableRAMBank[address - VRAM_MAX_ADDR] = value;
}
void MemoryMap::SetInternalRAM(uint16_t address, uint8_t value)
{
    //Log(std::string("Attempt to write INT RAM A:") +std::to_string(address) + std::string(" V: ") + std::to_string(value) );
    InternalRAM[address - SWITCHABLE_RAM_BANK_MAX_ADDR] = value;
}
void MemoryMap::SetEchoInternalRAM(uint16_t address, uint8_t value)
{
    //Log(std::string("Attempt to write ECHO Int RAM A:") +std::to_string(address) + std::string(" V: ") + std::to_string(value) );
    InternalRAM[address - INTERNAL_RAM_MAX_ADDR] = value;
}
void MemoryMap::SetSpriteAttributeMemory(uint16_t address, uint8_t value)
{
    //Log(std::string("Attempt to write Sprite Mem A:") +std::to_string(address) + std::string(" V: ") + std::to_string(value) );
    SpriteAttributeMemory[address - ECHO_INTERNAL_RAM_MAX_ADDR] = value;
}
void MemoryMap::SetFirstEmptyAddresses(uint16_t address, uint8_t value)
{
    //std::cout<<std::string("Attempt to write FirstEmptyAddresses A:") +std::to_string(address) + std::string(" V: ") + std::to_string(value)<<std::endl;
}
void MemoryMap::SetIOPorts(uint16_t address, uint8_t value)
{
    switch (address)
    {
    case 0xFF00:
        bus->Buttons->WriteRegister(value);
        break;
    case 0xFF01:
        break;
    case 0xFF02:
        break;
    case 0xFF04:
        bus->Cpu->ResetDividerRegister();
        break;
    case 0xFF05:
        bus->Cpu->SetTimerRegister(value);
        break;
    case 0xFF06:
        bus->Cpu->SetTimerModulo(value);
        break;
    case 0xFF07:
        bus->Cpu->SetTimerControl(value);
        break;
        //8-E don't do anything? Not documented
    case 0xFF0F:
        bus->Cpu->SetInterruptFlagRegister(value);
        break;
    case 0xFF10:
    case 0xFF11:
    case 0xFF12:
    case 0xFF13:
    case 0xFF14:
    case 0xFF16:
    case 0xFF17:
    case 0xFF18:
    case 0xFF19:
    case 0xFF1A:
    case 0xFF1B:
    case 0xFF1C:
    case 0xFF1D:
    case 0xFF1E:
    case 0xFF20:
    case 0xFF21:
    case 0xFF22:
    case 0xFF23:
    case 0xFF24:
    case 0xFF25:
    case 0xFF26:
    case 0xFF30:
        //Sound registers
        break;
    case 0xFF40:
        bus->Gpu->SetLCDC(value);
        break;
    case 0xFF41:
        bus->Gpu->SetLCDCStatus(value);
    case 0xFF42:
        bus->Gpu->ScrollY = value;
        break;
    case 0xFF43:
        bus->Gpu->ScrollX = value;
        break;
    case 0XFF44:
        bus->Gpu->ResetScanline();
        break;
    case 0xFF45:
        bus->Gpu->ScanlineCompare = value;
        break;
    case 0xFF47:
        bus->Gpu->SetBackgroundPaletteData(value);
        break;
    case 0xFF48:
        bus->Gpu->SetObject0PaletteData(value);
        break;
    case 0xFF49:
        bus->Gpu->SetObject1PaletteData(value);
        break;
    case 0xFF4A:
        bus->Gpu->SetWindowLocationY(value);
        break;
    case 0xFF4B:
        bus->Gpu->SetWindowLocationX(value);
        break;
    default:
            std::cout<<std::string("Attempt to write IOPorts, not yet implmeneted A:") <<std::hex<<address<<std::string(" V: ")<<std::hex<<value<<std::endl;
        break;
    }

}
void MemoryMap::SetSecondEmptyAddresses(uint16_t address, uint8_t value)
{
    if (address == 0xFF50)
    {
        this->readFromBios = false;
        std::cout<<"Changing to game cart"<<std::endl;
    }
    else
        std::cout<<std::string("Attempt to write SecondEmptyAddresses A:") +std::to_string(address) + std::string(" V: ") + std::to_string(value)<<std::endl;
}
void MemoryMap::SetSecondInternalRam(uint16_t address, uint8_t value)
{
    //Log(std::string("Attempt to write Stack RAM A:") +std::to_string(address) + std::string(" V: ") + std::to_string(value) );
    SecondInternalRam[address - SECOND_EMPTY_MAX_ADDR] = value;
}
void MemoryMap::SetInterruptRegister(uint8_t value){
    bus->Cpu->InterruptEnableFlag = value;
}

uint8_t MemoryMap::GetROMBank0(uint16_t address)
{
    if (readFromBios && address < bus->BIOS->size())
        return bus->BIOS->getCartData()[address];
    else
        return bus->CartData->getCartData()[address];
}
uint8_t MemoryMap::GetSwitchableROMBank(uint16_t address)
{
    return bus->CartData->getCartData()[address];
}
uint8_t MemoryMap::GetVRAM(uint16_t address)
{
    return VRAM[address - SWITCHABLE_ROM_BANK_MAX_ADDR];
}
uint8_t MemoryMap::GetSwitchableRAMBank(uint16_t address)
{
    return SwitchableRAMBank[address - VRAM_MAX_ADDR];
}
uint8_t MemoryMap::GetInternalRAM(uint16_t address)
{
    return InternalRAM[address - SWITCHABLE_RAM_BANK_MAX_ADDR];
}
uint8_t MemoryMap::GetEchoInternalRAM(uint16_t address)
{
    return InternalRAM[address - INTERNAL_RAM_MAX_ADDR];
}
uint8_t MemoryMap::GetSpriteAttributeMemory(uint16_t address)
{
    return SpriteAttributeMemory[address - ECHO_INTERNAL_RAM_MAX_ADDR];
}
uint8_t MemoryMap::GetFirstEmptyAddresses(uint16_t address)
{
    //Log(std::string("Attempt to read FirstEmptyAddresses A:") +std::to_string(address) );
    return INVALID_MEMORY_RESPONSE;
}
uint8_t MemoryMap::GetIOPorts(uint16_t address)
{
    switch (address)
    {
    case 0xFF00:
        return bus->Buttons->ReadRegister();
    case 0xFF40:
        return bus->Gpu->GetLCDC();
    case 0xFF41:
        return bus->Gpu->GetLCDCStatus();
    case 0xFF42:
        return bus->Gpu->ScrollY;
    case 0xFF43:
        return bus->Gpu->ScrollX;
    case 0xFF44:
        return bus->Gpu->Scanline();
    case 0xFF45:
        return bus->Gpu->ScanlineCompare;
    default:
        std::cout<<"Attempt to read IOPorts, not yet implmeneted A:" <<std::hex<<address<<std::endl;
        return INVALID_MEMORY_RESPONSE;
    }
}
uint8_t MemoryMap::GetSecondEmptyAddresses(uint16_t address)
{
    //Log(std::string("Attempt to write SecondEmptyAddresses A:") +std::to_string(address));
    return INVALID_MEMORY_RESPONSE;
}
uint8_t MemoryMap::GetSecondInternalRam(uint16_t address)
{
    return SecondInternalRam[address - SECOND_EMPTY_MAX_ADDR];
}
uint8_t MemoryMap::GetInterruptRegister()
{
    //todo
    return bus->Cpu->InterruptEnableFlag;
}
