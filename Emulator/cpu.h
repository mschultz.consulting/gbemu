#ifndef CPU_H
#define CPU_H
#include "includes.h"

class CPU
{
public:
    enum Interrupt{
        VBlank = 0x40,
        LCDCS = 0x48,
        TimerOverflow = 0x50,
        SerialTransferComplete = 0x58,
        Joypad = 0x60
    };
private:
    void ProcessInterrupts();
    uint8_t GetInterruptBit(Interrupt);
    enum TimerClockspeed{
        _4096Khz = 0x0,
        _262144Khz = 0b01,
        _65536Khz = 0b10,
        _16384Khz = 0b11
    };
    Bus* bus;
    std::vector<OPCode*> opcodeMap;

    uint8_t reg_A;
    uint8_t reg_B;
    uint8_t reg_C;
    uint8_t reg_D;
    uint8_t reg_E;
    //Flag register, only accessible through the flag functions
    uint8_t reg_F;
    uint8_t reg_H;
    uint8_t reg_L;
    uint16_t reg_PC;
    uint16_t reg_SP;


    uint8_t dividerRegister;
    uint8_t timerRegister;
    uint8_t timerModulo;
    bool timerEnabled;
    TimerClockspeed clockspeed;


    OPCode* NOPCode;
    OPCode* HaltCode;

public:
    enum Register16{
        BC = 0b000,
        DE = 0b001,
        HL = 0b010,
        SP = 0b011,
        PC = 0b100,
        AF = 0b111,
    };

    enum Register8: int{
        A = 0b111,
        B = 0b000,
        C = 0b001,
        D = 0b010,
        E = 0b011,
        H = 0b100,
        L = 0b101,
        M = 0b110, //This reads or writes to memory at location (HL)
        F = 0xFF
    };

    enum Flag{
        F_Z = 0b10000000,
        F_N = 0b01000000,
        F_H = 0b00100000,
        F_C = 0b00010000
    };



    CPU(Bus* bus);
    CPU(const CPU&);
    CPU& operator=(const CPU&);
    OPCode* DetermineOpcode(uint8_t instruction);
    void Tick();
    bool Debug;
    void PrintReg();
    unsigned long long Cycles;
    ~CPU();
    uint16_t Get16bitRegisterValue( Register16 reg);
    void AddTo16BitRegister(Register16 reg,signed char val);
    void Set16BitRegister(Register16 reg, uint16_t val);
    void Set8BitRegister(Register8 reg, uint8_t val);
    void AcceptInterrupt(Interrupt interrupt);


    bool InterruptMasterEnable;
    uint8_t InterruptFlagRegister;
    uint8_t InterruptEnableFlag;

    std::string Get8BitRegisterName(Register8 reg);
    uint8_t Get8BitRegister(Register8 reg);
    void DMA(uint16_t address, uint8_t val);
    uint8_t DMA(uint16_t address);
    bool GetFlag(Flag f){return reg_F & f;}
    void SetFlag(Flag f, bool set){if (set) reg_F |= f; else reg_F &= 0xFF^f;}
    uint8_t GetDividerRegister(){return dividerRegister;}
    void ResetDividerRegister(){dividerRegister = 0x0;}
    uint8_t GetTimerRegister(){return timerRegister;}
    void SetTimerRegister(uint8_t value){timerRegister = value;}
    uint8_t GetTimerModulo(){return timerModulo;}
    void SetTimerModulo(uint8_t value){timerModulo = value;}
    uint8_t GetTimerControl(){return 0b100*timerEnabled + (uint8_t)clockspeed;}
    void SetTimerControl(uint8_t value){clockspeed = (TimerClockspeed)(value&0b11); timerEnabled = ((value&0b100) != 0);}
    uint8_t GetInterruptFlagRegister(){return InterruptFlagRegister;}
    void SetInterruptFlagRegister(uint8_t value){ InterruptFlagRegister = value; }
};

#endif // CPU_H
