#include "opcodeparameters.h"

OPCodeParameters::OPCodeParameters(CPU *cpu)
    : OPCode(0)
    , HighByte(0)
    , LowByte(0)
    , Cpu(cpu)
{
}

