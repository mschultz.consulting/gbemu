#ifndef SOUNDCARD_H
#define SOUNDCARD_H
#include "includes.h"


class SoundCard
{
private:


public:
    class SoundMode{
    protected:
        enum WaveDuty{
            _12p5 = 0b00,
            _25 = 0b01,
            _50 = 0b10,
            _75 = 0b11
        };

        SoundMode();
        uint8_t sweepTime;
        bool sweepDecrease;
        uint8_t sweepShift;
        WaveDuty waveDuty;
        uint8_t soundLength;

    public:
        void SetSweepRegister(uint8_t value);
        uint8_t GetSweepRegister();
        void SetLengthWavePatternDuty(uint8_t value);
        uint8_t GetLengthWavePatternDuty();
    };
    SoundCard();

};

#endif // SOUNDCARD_H
