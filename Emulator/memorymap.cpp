#include "includes.h"

void ZeroBank(uint8_t* bank, uint16_t size)
{
    for (int i = 0; i < size; i++)
        bank[i] = 0;
}

MemoryMap::MemoryMap(Bus* val, bool allocate)
    : bus(val)
    , readFromBios(true)
    , VRAM(0x0)
    , SwitchableRAMBank(0x0)
    , InternalRAM(0x0)
    , SpriteAttributeMemory(0x0)
    , SecondInternalRam(0x0)
    , InterruptRegister(0x0)
{
    if (allocate)
    {
        //8k /ea
        VRAM = new unsigned char[VRAM_MAX_ADDR - SWITCHABLE_ROM_BANK_MAX_ADDR];
        SwitchableRAMBank = new unsigned char[SWITCHABLE_RAM_BANK_MAX_ADDR - VRAM_MAX_ADDR];
        InternalRAM = new unsigned char[INTERNAL_RAM_MAX_ADDR - SWITCHABLE_RAM_BANK_MAX_ADDR];
        //? Not sure why this is such an odd number, see the spec sheet
        SpriteAttributeMemory = new unsigned char[SPRITE_ATTRIBUTE_MEMORY_MAX_ADDR - INTERNAL_RAM_MAX_ADDR];
        //127 bytes
        SecondInternalRam = new unsigned char[SECOND_INTERNAL_RAM_MAX_ADDR - SECOND_EMPTY_MAX_ADDR];
        //ZeroBank(VRAM, VRAM_MAX_ADDR - SWITCHABLE_ROM_BANK_MAX_ADDR);
    }
}

MemoryMap& MemoryMap::operator =(const MemoryMap& copy)
{
    this->bus = copy.bus;
    this->InternalRAM = copy.InternalRAM;
    this->InterruptRegister = copy.InterruptRegister;
    this->readFromBios = copy.readFromBios;
    this->SecondInternalRam = copy.SecondInternalRam;
    this->SpriteAttributeMemory = copy.SpriteAttributeMemory;
    this->SwitchableRAMBank = copy.SwitchableRAMBank;
    this->VRAM = copy.VRAM;
    return *this;
}

MemoryMap::MemoryMap(const MemoryMap & copy)
    : bus ( copy.bus)
    , readFromBios ( copy.readFromBios)
    , VRAM ( copy.VRAM)
    , SwitchableRAMBank ( copy.SwitchableRAMBank)
    , InternalRAM ( copy.InternalRAM)
    , SpriteAttributeMemory ( copy.SpriteAttributeMemory)
    , SecondInternalRam ( copy.SecondInternalRam)
    , InterruptRegister ( copy.InterruptRegister)


{
}

MemoryMap::MemoryMap(Bus *val): MemoryMap::MemoryMap(val, true)
{

}

MemoryMap::~MemoryMap()
{
    delete VRAM;
    delete SwitchableRAMBank;
    delete InternalRAM;
    delete SpriteAttributeMemory;
    delete SecondInternalRam;
}

uint8_t MemoryMap::GetMemory(uint16_t address)
{
    uint8_t val = INVALID_MEMORY_RESPONSE;

    if (address < ROM_BANK_0_MAX_ADDR)
        val = this->GetROMBank0(address);
    else if (address < SWITCHABLE_ROM_BANK_MAX_ADDR)
        val = this->GetSwitchableROMBank(address);
    else if (address < VRAM_MAX_ADDR)
        val = this->GetVRAM(address);
    else if (address < SWITCHABLE_RAM_BANK_MAX_ADDR)
        val = this->GetSwitchableRAMBank(address);
    else if (address < INTERNAL_RAM_MAX_ADDR)
        val = this->GetInternalRAM(address);
    else if (address < ECHO_INTERNAL_RAM_MAX_ADDR)
        val = this->GetEchoInternalRAM(address);
    else if (address < SPRITE_ATTRIBUTE_MEMORY_MAX_ADDR)
        val = this->GetSpriteAttributeMemory(address);
    else if (address < FIRST_EMPTY_MAX_ADDR)
        val = this->GetFirstEmptyAddresses(address);
    else if (address < IO_PORTS_MAX_ADDR)
        val = this->GetIOPorts(address);
    else if (address < SECOND_EMPTY_MAX_ADDR)
        val = this->GetSecondEmptyAddresses(address);
    else if (address < SECOND_INTERNAL_RAM_MAX_ADDR)
        val = this->GetSecondInternalRam(address);
    else if (address == INTERRUPT_REGISTER_ADDR)
        val = this->GetInterruptRegister();
    else
        std::cout<<"Out of memory address"<<std::endl;
    return val;
}

void MemoryMap::SetMemory(uint16_t address, uint8_t data){

    if (address < ROM_BANK_0_MAX_ADDR)
        this->SetROMBank0(address, data);
    else if (address < SWITCHABLE_ROM_BANK_MAX_ADDR)
        this->SetSwitchableROMBank(address, data);
    else if (address < VRAM_MAX_ADDR)
        this->SetVRAM(address, data);
    else if (address < SWITCHABLE_RAM_BANK_MAX_ADDR)
        this->SetSwitchableRAMBank(address, data);
    else if (address < INTERNAL_RAM_MAX_ADDR)
        this->SetInternalRAM(address, data);
    else if (address < ECHO_INTERNAL_RAM_MAX_ADDR)
        this->SetEchoInternalRAM(address, data);
    else if (address < SPRITE_ATTRIBUTE_MEMORY_MAX_ADDR)
        this->SetSpriteAttributeMemory(address, data);
    else if (address < FIRST_EMPTY_MAX_ADDR)
        this->SetFirstEmptyAddresses(address, data);
    else if (address < IO_PORTS_MAX_ADDR)
        this->SetIOPorts(address, data);
    else if (address < SECOND_EMPTY_MAX_ADDR)
        this->SetSecondEmptyAddresses(address, data);
    else if (address < SECOND_INTERNAL_RAM_MAX_ADDR)
        this->SetSecondInternalRam(address, data);
    else if (address == INTERRUPT_REGISTER_ADDR)
        this->SetInterruptRegister(data);
    else
        std::cout<<"Out of addressable memory"<<std::endl;
}


void MemoryMap::PrintMemory(uint8_t* ptr, int len)
{
    using namespace std;
    cout<<std::hex;
    cout<<endl<<"0000 ";
    for (int i = 0; i < len; i++)
    {

        if (ptr[i] <= 0xF)
            cout<<"0";
        cout<<(int)ptr[i]<<" ";
        if (!((i+1)%16)){
            cout<<endl;
            if (i < 0x10)
                cout<<"000";
            else if (i < 0x100)
                cout<<"00";
            else if (i < 0x1000)
                cout<<"0";
            cout<<i<<" ";
        }
    }
    cout<<std::dec;
    cout<<endl<<endl;
}

void MemoryMap::PrintFullMemory()
{
    //PrintMemory(ROMBank0, 16*1024);
    //PrintMemory(SwitchableROMBank, 16*1024);
    PrintMemory(VRAM, 8*1024);
    //PrintMemory(SwitchableRAMBank, 8*1024);
    //PrintMemory(InternalRAM, 8*1024);
    //PrintMemory(SpriteAttributeMemory, 0xa0);
    //PrintMemory(SecondInternalRam, 127);
}

