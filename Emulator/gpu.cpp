#include "includes.h"
#include <allegro5/allegro.h>
#include <allegro5/allegro5.h>
#include <allegro5/allegro_primitives.h>

GPU::GPU(Bus *busv, ALLEGRO_DISPLAY* displ)
    : windowLocationX(WINDOW_LOCATION_X_INIT)
    , windowLocationY(WINDOW_LOCATION_Y_INIT)
    , bus(busv)
    , backgroundPaletteData(new PixelDotData())
    , object0PaletteData(new PixelDotData())
    , object1PaletteData(new PixelDotData())
    , scanline(VBLANK_START)
    , ScrollX(0)
    , ScrollY(0)
    , ScanlineCompareInterruptEnabled(false)
    , OAMInterruptEnabled(false)
    , VBlankInterruptEnabled(false)
    , HBlankInterruptEnabled(false)
    , al_pixels(NULL)
    , screenMultiplier(1)
    , ScanlineCompare(144)
    , currentMode(GPUMode::VBlank)
{

    this->SetScreenMultiplier(1, displ);
    if (display != NULL)
    {
        al_clear_to_color(al_map_rgb(0,0,0));
        al_flip_display();
    }
    pixels = new uint8_t[SCREEN_SIZE_X * SCREEN_SIZE_Y];
    //Set to vblank for initial setup
    scanline = VBLANK_START;
    SetLCDC(LCDC_INIT);
    scanline = 0;
}
void GPU::ResetScanline()
{
    scanline = 0;
}

uint8_t GPU::Scanline()
{
    return scanline;
}

uint8_t GPU::GetLCDCStatus()
{
    uint8_t val = 0x0;
    val += (ScanlineCompareInterruptEnabled << 6);
    val += (OAMInterruptEnabled << 5);
    val += (VBlankInterruptEnabled<<4);
    val += (HBlankInterruptEnabled<<3);
    val += (Scanline() == ScanlineCompare)<<2;
    val += (uint8_t)currentMode;
    return val;
}

void GPU::SetLCDCStatus(uint8_t val)
{
    ScanlineCompareInterruptEnabled = (val & 0b01000000) == 1;
    OAMInterruptEnabled = (val & 0b00100000) == 1;
    VBlankInterruptEnabled= (val & 0b00010000) == 1;
    HBlankInterruptEnabled = (val & 0b00001000) == 1;
}

void GPU::SetBackgroundPaletteData(uint8_t val)
{
    backgroundPaletteData->SetRegister(val);
}

uint8_t GPU::GetBackgroundPaletteData()
{
    return backgroundPaletteData->GetRegister();
}
void GPU::SetObject0PaletteData(uint8_t val)
{
    object0PaletteData->SetRegister(val);
}

uint8_t GPU::GetObject0PaletteData()
{
    return object0PaletteData->GetRegister();
}
void GPU::SetObject1PaletteData(uint8_t val)
{
    object1PaletteData->SetRegister(val);
}

uint8_t GPU::GetObject1PaletteData()
{
    return object1PaletteData->GetRegister();
}

uint8_t GPU::GetWindowLocationX()
{
    return windowLocationX;
}
uint8_t GPU::GetWindowLocationY()
{
    return windowLocationY;
}

void GPU::SetWindowLocationX(uint8_t value)
{
    windowLocationX = value;
}

void GPU::SetWindowLocationY(uint8_t value)
{
    windowLocationY = value;
}

void GPU::SetLCDC(uint8_t value)
{
    //Only allow enable/disable during vblank
    if (this->Scanline() >= VBLANK_START)
        enabled = BitEnabled(value, Bit::_7);
    if (BitEnabled(value, Bit::_6))
        windowTileMapDisplayLocation = TILE_MAP_DISPLAY_SELECT_1;
    else
        windowTileMapDisplayLocation = TILE_MAP_DISPLAY_SELECT_0;

    windowEnabled = BitEnabled(value, Bit::_5);

    if (BitEnabled(value, Bit::_4))
        bgAndWindowTileDataSelectLocation = BACKGROUND_AND_WINDOW_TILE_DATA_SELECT_1;
    else
        bgAndWindowTileDataSelectLocation = BACKGROUND_AND_WINDOW_TILE_DATA_SELECT_0;

    if (BitEnabled(value, Bit::_3))
        bgTileMapDisplaySelectLocation = TILE_MAP_DISPLAY_SELECT_1;
    else
        bgTileMapDisplaySelectLocation = TILE_MAP_DISPLAY_SELECT_0;

    if (BitEnabled(value, Bit::_2))
        spriteSize = SPRITE_SIZE_1;
    else
        spriteSize = SPRITE_SIZE_0;

    displaySprites = BitEnabled(value, Bit::_1);

    bgAndWindowDisplayEnabled = BitEnabled(value, Bit::_0);

}

uint8_t GPU::GetLCDC()
{
    uint8_t lcdc = 0x0;
    lcdc += enabled ? Bit::_7 : 0;
    lcdc += windowTileMapDisplayLocation == TILE_MAP_DISPLAY_SELECT_1 ? Bit::_6 : 0;
    lcdc += windowEnabled ? Bit::_5 : 0;
    lcdc += bgAndWindowTileDataSelectLocation == BACKGROUND_AND_WINDOW_TILE_DATA_SELECT_1 ? Bit::_4 : 0;
    lcdc += bgTileMapDisplaySelectLocation == TILE_MAP_DISPLAY_SELECT_1 ? Bit::_3 : 0;
    lcdc += spriteSize == SPRITE_SIZE_1 ? Bit::_2 : 0;
    lcdc += displaySprites ? Bit::_1 : 0;
    lcdc += bgAndWindowDisplayEnabled ? Bit::_0 : 0;
    return lcdc;
}
void GPU::DrawScanline()
{
    bool inWindow = windowEnabled && scanline <= windowLocationY;
    uint8_t yPosition;
    if (inWindow)
        yPosition = scanline - windowLocationY;
    else
        yPosition = ScrollY + scanline;

    uint16_t baselineTileAddress;
    if (inWindow)
        baselineTileAddress = windowTileMapDisplayLocation;
    else
        baselineTileAddress = bgTileMapDisplaySelectLocation;
    baselineTileAddress += ((yPosition/8)*32);

    for (int i = 0; i < SCREEN_SIZE_X; i++)
    {
        uint8_t xPosition = i + ScrollX;
        if (inWindow && i >= windowLocationX)
        {
            xPosition = i - windowLocationX;
        }

        if (xPosition > SCREEN_SIZE_X)
            continue;

        uint16_t tileAddress = baselineTileAddress + (xPosition/8);

        uint16_t tileNumber = bus->Map->GetMemory(tileAddress);

        uint16_t tileDataAddress = bgAndWindowTileDataSelectLocation;
        if (tileDataAddress == BACKGROUND_AND_WINDOW_TILE_DATA_SELECT_1 )
            tileDataAddress += tileNumber*16;
        else
            tileDataAddress += (signed short)((tileNumber+128)*16);

        uint8_t highLine = bus->Map->GetMemory(tileDataAddress + ((yPosition%8)*2));
        uint8_t lowLine  = bus->Map->GetMemory(tileDataAddress + ((yPosition%8)*2) + 1);

        uint8_t colorBit = (xPosition % 8);
        colorBit -=7;
        colorBit *= -1;

        uint8_t color = ((highLine >> colorBit)&0b1);
        color = color<<1;
        color += ((lowLine >> colorBit)&0b1);

        pixels[(scanline*SCREEN_SIZE_X)+xPosition] = backgroundPaletteData->GetShade((PixelDotData::Map)color);
    }
}

void GPU::Tick()
{
    if ( bus->Cpu->Cycles - cyclesAtLastScanlineWrite < H_BLANK_CYCLES)
    {
        if (currentMode != GPUMode::VBlank)
            currentMode = GPUMode::HBlank;
    }
    else if ( bus->Cpu->Cycles - cyclesAtLastScanlineWrite < PROCESSING_CYCLES)
    {
        if (currentMode != GPUMode::VBlank)
            currentMode = GPUMode::Processing;
    }
    else if ( bus->Cpu->Cycles - cyclesAtLastScanlineWrite < WRITING_CYCLES)
    {
        if (currentMode != GPUMode::VBlank)
            currentMode = GPUMode::Writing;
    }
    else
    {
        //Enter VBlank
        if (currentMode != GPUMode::VBlank)
            DrawScanline();

        cyclesAtLastScanlineWrite = bus->Cpu->Cycles;
        if (++scanline == ScanlineCompare)
            bus->Cpu->AcceptInterrupt(CPU::Interrupt::VBlank);

        if (scanline == VBLANK_START)
        {
            currentMode = GPUMode::VBlank;
            WriteToScreen();

            bus->Cpu->AcceptInterrupt(CPU::Interrupt::VBlank);
        }
        if (scanline == 153)
        {
            scanline = 0;
            currentMode = GPUMode::HBlank;
        }
    }
}

GPU::~GPU(){
    al_destroy_display(display);
    delete backgroundPaletteData;
    delete object0PaletteData;
    delete object1PaletteData;
    delete pixels;
}
void GPU::WriteToScreen()
{

    for (int x = 0; x < SCREEN_SIZE_X*screenMultiplier; x+= screenMultiplier)
    {
        for (int y = 0; y < SCREEN_SIZE_Y * screenMultiplier; y+= screenMultiplier)
        {
            for (int mx = 0; mx < screenMultiplier; mx++)
            {
                for (int my = 0; my < screenMultiplier; my++)
                {
                    uint8_t color = pixels[(x/screenMultiplier)+((y/screenMultiplier)*SCREEN_SIZE_X)];
                    al_pixels[x+mx+(y*SCREEN_SIZE_X*screenMultiplier+(my*screenMultiplier*SCREEN_SIZE_X))].color = al_map_rgb(color, color, color);
                }
            }
        }
    }
    if (display != NULL)
    {
        al_draw_prim(al_pixels, NULL, NULL, 0, (SCREEN_SIZE_X * screenMultiplier) * (SCREEN_SIZE_Y * screenMultiplier), ALLEGRO_PRIM_POINT_LIST);
        al_flip_display();
    }
}

void GPU::SetScreenMultiplier(uint8_t multiplier, ALLEGRO_DISPLAY *disp)
{
    display = disp;
    screenMultiplier = multiplier;
    delete al_pixels;
    al_pixels = new ALLEGRO_VERTEX[(SCREEN_SIZE_X * screenMultiplier) * (SCREEN_SIZE_Y * screenMultiplier)];
    for (int x = 0; x < (SCREEN_SIZE_X * screenMultiplier); x++)
    {
        for (int y = 0; y < (SCREEN_SIZE_Y * screenMultiplier); y++)
        {
            al_pixels[x + (y*(SCREEN_SIZE_X * screenMultiplier))].x = x;
            al_pixels[x + (y*(SCREEN_SIZE_X * screenMultiplier))].y = y;
            al_pixels[x + (y*(SCREEN_SIZE_X * screenMultiplier))].u = 0;
            al_pixels[x + (y*(SCREEN_SIZE_X * screenMultiplier))].v = 0;
            al_pixels[x + (y*(SCREEN_SIZE_X * screenMultiplier))].z = 0;
        }
    }
}
