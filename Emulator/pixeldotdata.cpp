#include "pixeldotdata.h"

PixelDotData::PixelDotData()
    : highData (PX_COLOR_BLACK)
    , midHighData(PX_HIGH_BIT_SET)
    , midLowData(PX_LOW_BIT_SET)
    , lowData(0x0)
{

}

void PixelDotData::SetRegister(uint8_t data)
{
    SetData(data, &highData, Bit::_7, Bit::_6);
    SetData(data, &midHighData, Bit::_5, Bit::_4);
    SetData(data, &midLowData, Bit::_3, Bit::_2);
    SetData(data, &lowData, Bit::_1, Bit::_0);
}
void PixelDotData::SetData(uint8_t reg, uint8_t *data, Bit high, Bit low)
{
    *data = 0xFF;
    *data -= BitEnabled(reg, high) ? PX_HIGH_BIT_SET : 0;
    *data -= BitEnabled(reg, low) ? PX_LOW_BIT_SET : 0;
}

uint8_t PixelDotData::addToRegister(uint8_t dataValue, Bit high, Bit low)
{
    uint8_t value = 0b0;
    if (dataValue <= PX_HIGH_BIT_SET)
        value += high;
    if (dataValue == PX_HIGH_BIT_SET || dataValue == 0 )
        value += low;
    return value;
}

uint8_t PixelDotData::GetRegister()
{
    uint8_t value = 0b0;
    value += addToRegister(highData, Bit::_7, Bit::_6);
    value += addToRegister(midHighData, Bit::_5, Bit::_4);
    value += addToRegister(midLowData, Bit::_3, Bit::_2);
    value += addToRegister(lowData, Bit::_1, Bit::_0);
    return value;
}

uint8_t PixelDotData::GetShade(Map map)
{
    switch(map){
    case Map::High:
        return highData;
    case Map::Low:
        return lowData;
    case Map::MidHigh:
        return midHighData;
    case Map::MidLow:
        return midLowData;
    default:
        return 0x0;
    }


}
