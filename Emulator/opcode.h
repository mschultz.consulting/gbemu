#ifndef OPCODE_H
#define OPCODE_H
#include "includes.h"
class CPU;

class OPCode
{
public:
    OPCode();
    virtual void Execute(OPCodeParameters params) = 0;
    // number of addl bytes
    virtual uint8_t numberAdditionalBytes() = 0;
    virtual uint8_t formatOpcode() = 0;
    virtual uint8_t checkOpcode() = 0;
    virtual std::string name() = 0;

    uint8_t* GetRegister(CPU* cpu, uint8_t bitmap);

    std::string GetRegisterName(uint8_t bitmap);
    const char* Get16BitRegisterName(uint8_t bitmap);
    CPU::Register8 ConvertTo8BitRegister(uint8_t val);
    CPU::Register16 ConvertTo16BitRegister(uint8_t val);
    void SetHalfCarry(CPU* cpu,uint8_t op1, uint8_t op2);
    virtual ~OPCode(){}
};

#endif // OPCODE_H
