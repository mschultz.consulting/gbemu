#ifndef INCLUDES
#define INCLUDES

#if defined __UINT32_MAX__ or UINT32_MAX
#include <inttypes.h>
#else
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;

#endif

#if defined DEBUG
#define WriteDebugLog(s){std::cout<<s<<std::endl;}
#else
#define WriteDebugLog(s){}
#endif
enum Bit{
    _0 = 0b00000001,
    _1 = 0b00000010,
    _2 = 0b00000100,
    _3 = 0b00001000,
    _4 = 0b00010000,
    _5 = 0b00100000,
    _6 = 0b01000000,
    _7 = 0b10000000,
};


#include <map>
#include <iostream>
#include <fstream>
#include <string>
#include <sys/types.h>
#include <time.h>
#include <sstream>
#include <vector>
#include "pixeldotdata.h"
class OPCodeParameters;
class PixelDotData;
class MemoryMap;
class Bus;
class CPU;
class GPU;
class Cart;
class Input;
class SoundCard;
class OPCode;
#include "memorymap.h"
#include "memorymaps/mbc3.h"
#include "bus.h"
#include "cpu.h"
#include "gpu.h"
#include "cart.h"
#include "input.h"
#include "soundcard.h"
#include "opcodeparameters.h"

#include "opcode.h"
#include "opcodes/jmpop.h"
#include "opcodes/bitmanipop.h"
#include "opcodes/mov.h"
#include "opcodes/mvi.h"
#include "opcodes/lxi.h"
#include "opcodes/logic.h"
#include "opcodes/ldoffset.h"
#include "opcodes/jr.h"
#include "opcodes/ldioconst.h"
#include "opcodes/ldioc.h"
#include "opcodes/offsetone.h"
#include "opcodes/ldax.h"
#include "opcodes/call.h"
#include "opcodes/checkcall.h"
#include "opcodes/push.h"
#include "opcodes/rotatea.h"
#include "opcodes/ret.h"
#include "opcodes/checkret.h"
#include "opcodes/pop.h"
#include "opcodes/nop.h"
#include "opcodes/addsub.h"
#include "opcodes/offset16.h"
#include "opcodes/rst.h"
#include "opcodes/logicimmediate.h"
#include "opcodes/ldxi.h"
#include "opcodes/jri.h"
#include "opcodes/jp.h"
#include "opcodes/addhl.h"
#include "opcodes/addsubimmediate.h"
#include "opcodes/cfop.h"
#include "opcodes/checkjp.h"
#include "opcodes/eidi.h"
#include "opcodes/reti.h"
#include "opcodes/ldmsp.h"
#include "opcodes/cpl.h"
#include "opcodes/addsp.h"
#include "opcodes/halt.h"
#include "opcodes/jphl.h"
#include "opcodes/ldsphl.h"
#include "opcodes/ldhlspi.h"
#include "opcodes/daa.h"

#include "gameboy.h"
bool BitEnabled(uint8_t value, Bit bit);
std::string currentDateTime();
void checkOps(CPU*);
#endif // INCLUDES

