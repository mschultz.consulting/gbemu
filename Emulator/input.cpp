#include "includes.h"
#include "allegro5/allegro.h"

Input::Input(Bus* busv)
    :  p14Set(true)
    , p15Set(true)
    , p10Set(false)
    , p11Set(false)
    , p12Set(false)
    , p13Set(false)
    , bus(busv)
{
}

uint8_t Input::ReadRegister(){
    return (p15Set*0x20 + p14Set*0x10 + p13Set*0x8 + p12Set*0x4 + p11Set*0x2 + p10Set*0x1);
}

void Input::WriteRegister(uint8_t value)
{
    p15Set = ((value&0x20) != 0);
    p14Set = ((value&0x10) != 0);
    p13Set = ((value&0x08) != 0);
    p12Set = ((value&0x04) != 0);
    p11Set = ((value&0x02) != 0);
    p10Set = ((value&0x01) != 0);
}

void Input::SetButton(Button value)
{
    if (value < Button::A && !p14Set)
        return;
    if (value > Button::Right && !p15Set)
        return;
    uint8_t tmp = value%4;
    switch (tmp)
    {
    case 0: p10Set = true; break;
    case 1: p11Set = true; break;
    case 2: p12Set = true; break;
    case 3: p13Set = true; break;
    }
}
void Input::Tick()
{
    ALLEGRO_KEYBOARD_STATE* state = new ALLEGRO_KEYBOARD_STATE();
    al_get_keyboard_state(state);
    for (auto key : map)
    {
        if (al_key_down(state, key.second))
        {
            SetButton(key.first);
            bus->Cpu->AcceptInterrupt(CPU::Interrupt::Joypad);
        }
    }
    delete state;
}

Input::~Input()
{
}
