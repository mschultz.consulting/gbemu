#include "includes.h"

OPCode::OPCode()
{

}

std::string OPCode::GetRegisterName(uint8_t bitmap){

    return CPU(NULL).Get8BitRegisterName(ConvertTo8BitRegister(bitmap));
}

const char* OPCode::Get16BitRegisterName(uint8_t bitmap)
{
    if ((bitmap ^ 0b11)==0)
        return "SP";
    if ((bitmap ^ 0b10)==0)
        return "HL";
    if ((bitmap ^ 0b01)==0)
        return "DE";
    return "BC";
}

CPU::Register8 OPCode::ConvertTo8BitRegister(uint8_t val)
{
    return (CPU::Register8)val;
}
CPU::Register16 OPCode::ConvertTo16BitRegister(uint8_t val)
{
    return (CPU::Register16)val;

}

void OPCode::SetHalfCarry(CPU* cpu, uint8_t op1, uint8_t op2)
{
    uint8_t o1 = op1&0xF, o2 = op2&0xF;
    cpu->SetFlag(CPU::Flag::F_H, o1+o2 > 0xF);

}
