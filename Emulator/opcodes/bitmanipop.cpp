#include "../includes.h"

BitManipOP::BitManipOP()
{

}

void BitManipOP::Execute(OPCodeParameters params)
{
    uint8_t ins = (params.LowByte & 0b11000000)>>6;
    CPU::Register8 regRef = ConvertTo8BitRegister(params.LowByte & 0b00000111);
    (this->*(funct[ins]))(params.Cpu, params.LowByte, regRef);
}

void BitManipOP::Execute00(CPU *cpu, uint8_t bit, CPU::Register8 reg)
{
    if (bit < 0xF)
        RotateWithCarry(cpu, bit > 0x7 , reg);
    else if (bit < 0x1F)
        RotateWithoutCarry(cpu, bit > 0x17, reg);
    else if (bit < 0x2F)
        ShiftArithmetic(cpu, bit > 0x27, reg);
    else if (bit < 0x37)
        Swap(cpu, reg);
    else
        ShiftRightLogic(cpu, reg);
    cpu->SetFlag(CPU::Flag::F_H, false);
    cpu->SetFlag(CPU::Flag::F_N, false);
}
void BitManipOP::RotateWithoutCarry(CPU* cpu, bool right, CPU::Register8 reg)
{
    uint8_t carry = 0;
    uint8_t oldCarry = cpu->GetFlag(CPU::Flag::F_C);
    uint8_t val = cpu->Get8BitRegister(reg);
    if (right)
    {
        carry = (val & 0x1);
        val = (val >> 1) + (oldCarry <<7);
    }
    else
    {
        carry = ((val) >> 7);
        val = (val << 1) + oldCarry;
    }

    cpu->SetFlag(CPU::Flag::F_C, carry == 0x1);
    cpu->Set8BitRegister(reg, val);
    cpu->SetFlag(CPU::Flag::F_Z, val == 0);
}

void BitManipOP::RotateWithCarry(CPU* cpu, bool right, CPU::Register8 reg)
{
    uint8_t carry = 0;
    uint8_t val = cpu->Get8BitRegister(reg);
    if (right)
    {
        carry = (val & 0x1);
        val = (val >> 1) + (carry <<7);
    }
    else
    {
        carry = ((val) >> 7);
        val = (val << 1) + carry ;
    }

    cpu->SetFlag(CPU::Flag::F_C, carry == 0x1);
    cpu->Set8BitRegister(reg, val);
    cpu->SetFlag(CPU::Flag::F_Z, val == 0);
}
void BitManipOP::ShiftArithmetic(CPU *cpu, bool right, CPU::Register8 reg)
{
    uint8_t val = cpu->Get8BitRegister(reg);
    if (right)
    {
        cpu->SetFlag(CPU::Flag::F_C, (val&0x1) == 1);
        val = val >> 1;
        val += ((val & 0b01000000)<<1);
    }
    else
    {
        cpu->SetFlag(CPU::Flag::F_C, (val&0x80) == 0x80);
        val = val << 1;
    }
    cpu->Set8BitRegister(reg, val);
    cpu->SetFlag(CPU::Flag::F_Z, !val);
    cpu->SetFlag(CPU::Flag::F_N, false);
    cpu->SetFlag(CPU::Flag::F_H, false);
}

void BitManipOP::Swap(CPU *cpu, CPU::Register8 reg)
{
    uint8_t val = cpu->Get8BitRegister(reg);
    cpu->Set8BitRegister(reg, ((val & 0xF)<<4) + ((val & 0xF0)>>4));
    cpu->SetFlag(CPU::Flag::F_Z, val == 0);
    cpu->SetFlag(CPU::Flag::F_C, false);
    cpu->SetFlag(CPU::Flag::F_H, false);
    cpu->SetFlag(CPU::Flag::F_N, false);
}
void BitManipOP::ShiftRightLogic(CPU *cpu, CPU::Register8 reg)
{
    uint8_t val = cpu->Get8BitRegister(reg);
    cpu->SetFlag(CPU::Flag::F_C, (val&0x1) == 1);
    val = val >> 1;
    cpu->Set8BitRegister(reg, val);
    cpu->SetFlag(CPU::Flag::F_Z, !val);
    cpu->SetFlag(CPU::Flag::F_C, false);
    cpu->SetFlag(CPU::Flag::F_H, false);
    cpu->SetFlag(CPU::Flag::F_N, false);
}

void BitManipOP::ZBit(CPU *cpu, uint8_t bit,  CPU::Register8  reg)
{
    cpu->SetFlag(CPU::Flag::F_H, true);
    cpu->SetFlag(CPU::Flag::F_N, false);
    cpu->SetFlag(CPU::Flag::F_Z, !(bool)(cpu->Get8BitRegister(reg) & ByteMap[((bit & 0b00111000) >> 3)]));
}

void BitManipOP::RES(CPU *cpu, uint8_t bit,  CPU::Register8  reg)
{
    cpu->Set8BitRegister(reg,(ByteMap[((bit & 0b00111000) >> 3)]^255)&(cpu->Get8BitRegister(reg)));
}

void BitManipOP::SET(CPU *cpu, uint8_t bit,  CPU::Register8  reg)
{
    cpu->Set8BitRegister(reg,ByteMap[((bit & 0b00111000) >> 3)]&(cpu->Get8BitRegister(reg)));
}
