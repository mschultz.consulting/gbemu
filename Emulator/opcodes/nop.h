#ifndef NOP_H
#define NOP_H
#include "../includes.h"

class NOP : public OPCode
{
public:
    NOP();
    uint8_t numberAdditionalBytes(){return 0;}
    uint8_t formatOpcode(){return 0xFF;}
    uint8_t checkOpcode(){return 0x0;}
    std::string name(){return "NOP";}
    void Execute(OPCodeParameters params);

};

#endif // NOP_H
