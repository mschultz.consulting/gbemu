#include "../includes.h"

LDHLSPI::LDHLSPI()
{

}

void LDHLSPI::Execute(OPCodeParameters params)
{
    uint32_t val = params.Cpu->Get16bitRegisterValue(CPU::Register16::SP);
    signed char offset = (signed char)params.LowByte;
    if (offset < 0)
    {
        uint8_t absOffset = abs(offset);
        params.Cpu->SetFlag(CPU::Flag::F_C, absOffset > val);
        params.Cpu->SetFlag(CPU::Flag::F_H, (absOffset &0xF) > (val &0xF));
    }
    else
    {
        params.Cpu->SetFlag(CPU::Flag::F_C, ((val+offset)&0x10000) != 0);
        params.Cpu->SetFlag(CPU::Flag::F_H, (((val&0xFFF)+offset)&0x1000) != 0);
    }
    params.Cpu->SetFlag(CPU::Flag::F_Z, false);
    params.Cpu->SetFlag(CPU::Flag::F_N, false);
    params.Cpu->Set16BitRegister(CPU::Register16::HL, val + offset);
}
