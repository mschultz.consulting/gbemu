#ifndef ROTATE_H
#define ROTATE_H
#include "../includes.h"

class RotateA : public BitManipOP
{
public:
    RotateA();
    uint8_t numberAdditionalBytes(){return 0;}
    uint8_t formatOpcode(){return 0b11100111;}
    uint8_t checkOpcode(){return 0b00000111;}
    void Execute(OPCodeParameters params);
};

#endif // ROTATE_H
