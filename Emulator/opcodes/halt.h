#ifndef HALT_H
#define HALT_H
#include "../includes.h"

class HALT : public OPCode
{
public:
    HALT();

    void Execute(OPCodeParameters params);
    uint8_t numberAdditionalBytes() { return 0;}
    uint8_t formatOpcode() { return 0xFF;}
    uint8_t checkOpcode(){return    0x76;}
    std::string name(){return "HALT";}
};

#endif // HALT_H
