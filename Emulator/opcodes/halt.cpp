#include "../includes.h"

HALT::HALT()
{

}

void HALT::Execute(OPCodeParameters params)
{
    params.Cpu->AddTo16BitRegister(CPU::Register16::PC, -1);
}
