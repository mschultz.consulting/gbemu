#include "../includes.h"

CALL::CALL()
{

}

void CALL::Execute(OPCodeParameters params)
{
    uint16_t pc = params.Cpu->Get16bitRegisterValue(CPU::Register16::PC);
    uint16_t sp = params.Cpu->Get16bitRegisterValue(CPU::Register16::SP);
    std::cout<<"Call made from "<<std::hex<<(int)pc;
    params.Cpu->DMA(sp-1, (uint8_t)(pc>>8));
    params.Cpu->DMA(sp-2, (uint8_t)(pc));
    params.Cpu->Set16BitRegister(CPU::Register16::SP, sp - 2);
    pc = params.LowByte;
    pc += (params.HighByte<<8);
    std::cout<<" to "<<std::hex<<(int)pc<<std::endl;
    params.Cpu->Set16BitRegister(CPU::Register16::PC, pc);

}
