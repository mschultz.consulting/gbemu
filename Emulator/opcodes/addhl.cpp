#include "../includes.h"

ADDHL::ADDHL()
{

}

void ADDHL::Execute(OPCodeParameters params)
{

    uint16_t reg_val = params.Cpu->Get16bitRegisterValue((CPU::Register16)((params.OPCode & 0b00110000)>>4));
    uint16_t hl = params.Cpu->Get16bitRegisterValue(CPU::Register16::HL);

    params.Cpu->SetFlag(CPU::Flag::F_C, ((uint32_t)reg_val + (uint32_t)hl) > 0xFFFF);
    params.Cpu->SetFlag(CPU::Flag::F_N, 0);
    params.Cpu->SetFlag(CPU::Flag::F_H, ((reg_val & 0xFFF ) + (hl & 0xFFF)) > 0xFFF);


    params.Cpu->Set16BitRegister(CPU::Register16::HL, hl + reg_val);
}
