#ifndef BITMANIPOP_H
#define BITMANIPOP_H
#include "../includes.h"

class BitManipOP: public OPCode
{
public:
    BitManipOP();
    uint8_t numberAdditionalBytes(){return 1;}

    uint8_t checkOpcode(){return 0xCB;}
    uint8_t formatOpcode(){return  0xFF;}
    std::string name(){return "BitManip";}

    void Execute(OPCodeParameters params);
    void ZBit(CPU* cpu, uint8_t bit, CPU::Register8 reg);
    void RES(CPU* cpu, uint8_t bit, CPU::Register8 reg);
    void SET(CPU* cpu, uint8_t bit, CPU::Register8 reg);
    void Execute00(CPU* cpu, uint8_t bit, CPU::Register8 reg);
    void RotateWithCarry(CPU* cpu, bool right, CPU::Register8 reg);
    void RotateWithoutCarry(CPU* cpu, bool right, CPU::Register8 reg);
    void ShiftArithmetic(CPU* cpu, bool right, CPU::Register8 reg);
    void Swap(CPU* cpu, CPU::Register8 reg);
    void ShiftRightLogic(CPU* cpu, CPU::Register8 reg);
private:

    uint8_t ByteMap[8] = {
        1,
        2,
        4,
        8,
        16,
        32,
        64,
        128
    };
    typedef void (BitManipOP::*ptrfn)(CPU*,uint8_t,CPU::Register8);
    ptrfn funct[4] = {
        &BitManipOP::Execute00,
        &BitManipOP::ZBit,
        &BitManipOP::RES,
        &BitManipOP::SET
    };
};

#endif // BITMANIPOP_H
