#ifndef CPL_H
#define CPL_H
#include "../includes.h"

class CPL : public OPCode
{
public:
    CPL();
    void Execute(OPCodeParameters params);
    uint8_t numberAdditionalBytes() { return 0;}
    uint8_t formatOpcode() { return 0xFF;}
    uint8_t checkOpcode(){return    0x2F;}
    std::string name(){return "CPL";}
};

#endif // CPL_H
