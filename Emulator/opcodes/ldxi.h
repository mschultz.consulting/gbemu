#ifndef LDXI_H
#define LDXI_H
#include "../includes.h"

class LDXI : public OPCode
{
public:
    LDXI();
    uint8_t numberAdditionalBytes(){return 2;}
    uint8_t formatOpcode(){return 0b11101111;}
    uint8_t checkOpcode(){return 0b11101010;}
    std::string name(){return "LDXI";}
    void Execute(OPCodeParameters params);
};

#endif // LDXI_H
