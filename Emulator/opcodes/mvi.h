#ifndef MVI_H
#define MVI_H
#include "../includes.h"

class MVI : public OPCode
{
public:
    MVI();
    uint8_t numberAdditionalBytes(){return 1;}
    void Execute(OPCodeParameters params);
    uint8_t formatOpcode(){return 0b11000111;}
    uint8_t checkOpcode(){return 0b00000110;}
    std::string name(){return "MVI";}
};

#endif // MVI_H
