#ifndef CFOP_H
#define CFOP_H
#include "../includes.h"

class CFOp : public OPCode
{
public:
    CFOp();
    uint8_t numberAdditionalBytes(){return 0;}
    uint8_t formatOpcode(){return 0b11110111;}
    uint8_t checkOpcode(){return  0b00110111;}
    void Execute(OPCodeParameters params);
    std::string name(){return "CarryFlag";}

};

#endif // CFOP_H
