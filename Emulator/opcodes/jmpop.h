#ifndef JMPOP_H
#define JMPOP_H
#include "../includes.h"

class JMPOP
{
public:
    virtual ~JMPOP(){}
    JMPOP();
protected:
    enum JMPType{
        NZ = 0b00,
        Z  = 0b01,
        NC = 0b10,
        C  = 0b11,
    };
    std::string JMPTypeStrings[4] = {
        "NZ",
        "Z",
        "NC",
        "C"};

    JMPType DetermineJMPType(uint8_t bitflag);
    bool  ShouldJump(JMPType type, CPU*cpu);
};

#endif // JMPOP_H
