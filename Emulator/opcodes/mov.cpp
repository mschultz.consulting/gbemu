#include "../includes.h"

MOV::MOV()
{

}

void MOV::Execute(OPCodeParameters params)
{
    CPU::Register8 bitmap_source = ConvertTo8BitRegister(params.OPCode & 0b00000111);
    CPU::Register8 bitmap_dest =ConvertTo8BitRegister( (params.OPCode & 0b00111000)>>3);
    params.Cpu->Set8BitRegister(bitmap_dest, params.Cpu->Get8BitRegister(bitmap_source));
}
