#ifndef LOGICIMMEDIATE_H
#define LOGICIMMEDIATE_H
#include "../includes.h"

class LogicImmediate : public OPCode
{
public:
    LogicImmediate();
    uint8_t numberAdditionalBytes(){return 1;}
    uint8_t formatOpcode(){return 0b11100111;}
    uint8_t checkOpcode(){return 0b11100110;}
    std::string name(){return "Logic Immediate";}
    void Execute(OPCodeParameters params);
};

#endif // LOGICIMMEDIATE_H
