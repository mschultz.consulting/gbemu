#include "../includes.h"

DAA::DAA()
{

}

void DAA::Execute(OPCodeParameters params)
{
    uint16_t val = params.Cpu->Get8BitRegister(CPU::Register8::A);
    if (!params.Cpu->GetFlag(CPU::Flag::F_N))
    {
        if (params.Cpu->GetFlag(CPU::Flag::F_C) || val > 0x99)
        {
            val += 0x60;
            params.Cpu->SetFlag(CPU::Flag::F_C,true);
        }
        if (params.Cpu->GetFlag(CPU::Flag::F_H) || (val&0xF)>0x9)
        {
            val += 0x6;
        }
    }
    else
    {
        if (params.Cpu->GetFlag(CPU::Flag::F_C))
        {
            val -= 0x60;
        }
        if (params.Cpu->GetFlag(CPU::Flag::F_H) )
        {
            val =(val- 0x6)&0xFF;
        }
    }
    params.Cpu->SetFlag(CPU::Flag::F_H, false);
    params.Cpu->SetFlag(CPU::Flag::F_Z, (val&0xFF) == 0);
    params.Cpu->Set8BitRegister(CPU::Register8::A, (uint8_t)(val&0xFF));
}
