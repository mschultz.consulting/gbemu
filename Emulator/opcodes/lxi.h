#ifndef LXI_H
#define LXI_H
#include "../includes.h"

class LXI : public OPCode
{
public:
    LXI();
    uint8_t numberAdditionalBytes(){return 2;}
    void Execute(OPCodeParameters params);
    uint8_t formatOpcode(){return 0b11001111;}
    uint8_t checkOpcode(){return 0b00000001;}
std::string name(){return "LXI";}
};

#endif // LXI_H
