#ifndef RET_H
#define RET_H
#include "../includes.h"

class RET : public OPCode
{
public:
    RET();
    uint8_t numberAdditionalBytes(){return 0;}
    uint8_t formatOpcode(){return 0b11111111;}
    uint8_t checkOpcode(){return 0xC9;}
    void Execute(OPCodeParameters params);
    std::string name(){return "RET";}

};

#endif // RET_H
