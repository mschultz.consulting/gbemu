#ifndef POP_H
#define POP_H
#include "../includes.h"

class POP : public OPCode
{
public:
    POP();
    uint8_t numberAdditionalBytes(){return 0;}
    uint8_t formatOpcode(){return 0b11001111;}
    uint8_t checkOpcode(){return 0b11000001;}
    std::string name(){return "POP";}
    void Execute(OPCodeParameters params);
};

#endif // POP_H
