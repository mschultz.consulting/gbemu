#include "../includes.h"

ADDSUB::ADDSUB()
{

}

void ADDSUB::Execute(OPCodeParameters params)
{
    uint8_t carry = params.Cpu->GetFlag(CPU::Flag::F_C);
    params.Cpu->SetFlag(CPU::Flag::F_N, false);
    uint16_t source = params.Cpu->Get8BitRegister(CPU::Register8::A);
    uint8_t reg = params.Cpu->Get8BitRegister(ConvertTo8BitRegister(params.OPCode&0b00000111));
    if (( params.OPCode & 0b00001000) ==0)
        carry = 0;
    if ((params.OPCode & 0b00010000)!= 0)
    {
        params.Cpu->SetFlag(CPU::Flag::F_H, ((reg&0xF)+carry) > (source&0xF));
        source -= reg;
        source -= carry;
        params.Cpu->SetFlag(CPU::Flag::F_N, true);

    }
    else
    {
        params.Cpu->SetFlag(CPU::Flag::F_H, (source & 0xF) + (reg  & 0xF) + carry > 0xF);
        source += reg;
        source += carry;
    }

    params.Cpu->SetFlag(CPU::Flag::F_Z, ((source & 0x00FF) == 0));
    params.Cpu->SetFlag(CPU::Flag::F_C, ((source & 0x0100) != 0)|((source & 0x8000) != 0));
    params.Cpu->Set8BitRegister(CPU::Register8::A, (source&0x00FF));
}


