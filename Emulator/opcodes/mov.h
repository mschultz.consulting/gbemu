#ifndef MOV_H
#define MOV_H
#include "../includes.h"
class CPU;

class MOV: public OPCode{
public:
    MOV();
    void Execute(OPCodeParameters params);
    uint8_t numberAdditionalBytes(){return 0;}
    uint8_t formatOpcode(){return 0b11000000;}
    uint8_t checkOpcode(){return 0b01000000;}
    std::string name(){return "MOV";}
};

#endif // MOV_H
