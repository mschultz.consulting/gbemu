#ifndef ADDSUBIMMEDIATE_H
#define ADDSUBIMMEDIATE_H
#include "../includes.h"

class ADDSUBIMMEDIATE : public OPCode
{
public:
    ADDSUBIMMEDIATE();
    virtual uint8_t numberAdditionalBytes(){return 1;}
    virtual uint8_t formatOpcode(){return 0b11100111;}
    virtual uint8_t checkOpcode(){return 0b11000110;}
    virtual void Execute(OPCodeParameters params);
    virtual std::string name(){return "ADD/SUB Immediate";}
};

#endif // ADDSUBIMMEDIATE_H
