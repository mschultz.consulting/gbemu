#ifndef CALLOPCODE_H
#define CALLOPCODE_H
#include "../includes.h"

class CALL : public OPCode
{
public:
    CALL();
    uint8_t numberAdditionalBytes(){return 2;}
    uint8_t formatOpcode(){return 0b11111111;}
    uint8_t checkOpcode(){return 0xCD;}
    std::string name(){return "CALL";}
    void Execute(OPCodeParameters params);
};

#endif // CALLOPCODE_H
