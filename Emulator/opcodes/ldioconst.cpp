#include "../includes.h"

LDIOConst::LDIOConst()
{

}

void LDIOConst::Execute(OPCodeParameters params)
{
    uint16_t address = 0xFF00 +params.LowByte;

    if ((params.OPCode & 0b00010000) == 0){
        params.Cpu->DMA(address, params.Cpu->Get8BitRegister(CPU::Register8::A));
    }
    else{
        params.Cpu->Set8BitRegister(CPU::Register8::A, params.Cpu->DMA(address));
    }
}
