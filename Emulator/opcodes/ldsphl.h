#ifndef LDSPHL_H
#define LDSPHL_H
#include "../includes.h"

class LDSPHL : public OPCode
{
public:
    LDSPHL();
    uint8_t numberAdditionalBytes(){return 0;}
    uint8_t formatOpcode(){return 0xFF;}
    uint8_t checkOpcode() {return 0xF9;}
    std::string name(){return "LDSPHL";}
    void Execute(OPCodeParameters params);
};

#endif // LDSPHL_H
