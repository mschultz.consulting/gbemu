#ifndef LDIOC_H
#define LDIOC_H
#include "../includes.h"

class LDIOC : public OPCode
{
public:
    LDIOC();
    uint8_t numberAdditionalBytes() {return 0;}
    void Execute(OPCodeParameters params);
    uint8_t formatOpcode(){return 0b11101111;}
    uint8_t checkOpcode(){return 0b11100010;}
    std::string name(){return "LDIO-C";}
};

#endif // LDIOC_H
