#ifndef CHECKRET_H
#define CHECKRET_H
#include "../includes.h"

class CheckRET : public RET, public JMPOP
{
public:
    CheckRET();
    uint8_t formatOpcode(){return 0b11100111;}
    uint8_t checkOpcode(){return 0b11000000;}
    void Execute(OPCodeParameters params);
    std::string name(){return "CheckRET";}
    virtual ~CheckRET() {}

};

#endif // CHECKRET_H
