#ifndef ADDSP_H
#define ADDSP_H
#include "../includes.h"

class ADDSP : public OPCode
{
public:
    ADDSP();

    void Execute(OPCodeParameters params);
    uint8_t numberAdditionalBytes() { return 1;}
    uint8_t formatOpcode() { return 0xFF;}
    uint8_t checkOpcode(){return    0xE8;}
    std::string name(){return "ADD SP,D8";}
};

#endif // ADDSP_H
