#include "ldxi.h"

LDXI::LDXI()
{

}

void LDXI::Execute(OPCodeParameters params)
{
    uint16_t memLoc = params.HighByte;
    memLoc = memLoc<<8;
    memLoc +=params.LowByte;
    if ((params.OPCode&0b00010000) != 0)
    {
        params.Cpu->Set8BitRegister(CPU::Register8::A, params.Cpu->DMA(memLoc));
    }
    else
    {
       params.Cpu->DMA(memLoc, params.Cpu->Get8BitRegister(CPU::Register8::A));
    }
}
