#include "checkjp.h"

CheckJP::CheckJP()
{

}

void CheckJP::Execute(OPCodeParameters params)
{
    JMPType jmp = DetermineJMPType(params.OPCode>>3);
    if (ShouldJump(jmp, params.Cpu))
        JP::Execute(params);

}

