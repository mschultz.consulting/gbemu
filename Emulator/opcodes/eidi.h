#ifndef EIDI_H
#define EIDI_H
#include "../includes.h"

class EIDI : public OPCode
{
public:
    EIDI();
    uint8_t numberAdditionalBytes(){return 0;}
    uint8_t formatOpcode() {return 0b11110111;}
    uint8_t checkOpcode(){return 0b11110011;}
    std::string name(){return "EIDI";}
    void Execute(OPCodeParameters params);

};

#endif // EIDI_H
