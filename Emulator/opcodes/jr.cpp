#include "../includes.h"

JR::JR()
{

}

void JR::Execute(OPCodeParameters params)
{
    signed char offset = params.LowByte;
    JMPType jmp = DetermineJMPType(params.OPCode>>3);
    if (ShouldJump(jmp, params.Cpu))
        params.Cpu->AddTo16BitRegister(CPU::Register16::PC,offset);
}
