#ifndef PUSH_H
#define PUSH_H
#include "../includes.h"

class PUSH : public OPCode
{
public:
    PUSH();
    uint8_t numberAdditionalBytes(){return 0;}
    uint8_t formatOpcode(){return 0b11001111;}
    uint8_t checkOpcode(){return 0b11000101;}
    std::string name(){return "PUSH";}
    void Execute(OPCodeParameters params);
};

#endif // PUSH_H
