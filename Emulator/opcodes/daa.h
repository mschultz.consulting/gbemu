#ifndef DAA_H
#define DAA_H
#include "../includes.h"

class DAA : public OPCode
{
public:
    DAA();

    void Execute(OPCodeParameters params);
    uint8_t numberAdditionalBytes() { return 0;}
    uint8_t formatOpcode() { return 0xFF;}
    uint8_t checkOpcode(){return    0x27;}
    std::string name(){return "DAA";}
};

#endif // DAA_H
