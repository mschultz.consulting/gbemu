#include "../includes.h"

LDAX::LDAX()
{

}

void LDAX::Execute(OPCodeParameters params)
{
    CPU::Register16 address;
    if ((params.OPCode&0b00010000) == 0)
        address = CPU::Register16::BC;
    else
        address = CPU::Register16::DE;
    if ((params.OPCode&0b00001000) == 0)
        params.Cpu->DMA(params.Cpu->Get16bitRegisterValue(address), params.Cpu->Get8BitRegister(CPU::Register8::A));
    else
        params.Cpu->Set8BitRegister(CPU::Register8::A, params.Cpu->DMA(params.Cpu->Get16bitRegisterValue(address)));
}
