#include "../includes.h"

RET::RET()
{

}

void RET::Execute(OPCodeParameters params)
{
    uint16_t sp = params.Cpu->Get16bitRegisterValue(CPU::Register16::SP);
    uint16_t pc = params.Cpu->DMA(sp+1);
    pc = (pc <<8);
    pc += params.Cpu->DMA(sp);
    std::cout<<"Returning to "<<std::hex<<pc<<std::endl;
    params.Cpu->Set16BitRegister(CPU::Register16::PC, pc);
    params.Cpu->Set16BitRegister(CPU::Register16::SP, sp+2);

}
