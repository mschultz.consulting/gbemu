#ifndef LDIOCONST_H
#define LDIOCONST_H
#include "../includes.h"

class LDIOConst : public OPCode
{
public:
    LDIOConst();
    uint8_t numberAdditionalBytes() { return 1;}
    void Execute(OPCodeParameters params);
    uint8_t formatOpcode(){return 0b11101111;}
    uint8_t checkOpcode(){return 0b11100000;}
    std::string name(){return "LDIO Const";}
};

#endif // LDIOCONST_H
