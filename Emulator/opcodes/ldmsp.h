#ifndef LDMSP_H
#define LDMSP_H
#include "../includes.h"

class LDMSP : public OPCode
{
public:
    LDMSP();
    void Execute(OPCodeParameters params);
    uint8_t numberAdditionalBytes() { return 2;}
    uint8_t formatOpcode() { return 0xFF;}
    uint8_t checkOpcode(){return    0x08;}
    std::string name(){return "LDM,SP";}

};

#endif // LDMSP_H
