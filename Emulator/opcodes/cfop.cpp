#include "../includes.h"

CFOp::CFOp()
{

}

void CFOp::Execute(OPCodeParameters params)
{
    if ((params.OPCode & 0b00001000) == 0)
        params.Cpu->SetFlag(CPU::F_C, true);
    else
        params.Cpu->SetFlag(CPU::F_C, !params.Cpu->GetFlag(CPU::F_C));
    params.Cpu->SetFlag(CPU::Flag::F_N, false);
    params.Cpu->SetFlag(CPU::Flag::F_H, false);
}
