#include "../includes.h"

MVI::MVI()
{

}

void MVI::Execute(OPCodeParameters params)
{
    CPU::Register8 dest = ConvertTo8BitRegister((params.OPCode & 0b00111000)>>3);
    params.Cpu->Set8BitRegister(dest,params.LowByte);
}
