#include "../includes.h"

Offset16::Offset16()
{

}

void Offset16::Execute(OPCodeParameters params)
{
    CPU::Register16 source = (CPU::Register16)((params.OPCode&0b00110000)>>4);
    signed char offset = 0;
    if((params.OPCode&0b00001000) == 0)
    {
        offset = 1;
    }
    else
    {
        offset = -1;
    }

    uint16_t val =  params.Cpu->Get16bitRegisterValue(source)+offset;
    params.Cpu->Set16BitRegister(source,val);
}
