#include "../includes.h"
PUSH::PUSH()
{

}

void PUSH::Execute(OPCodeParameters params)
{

    CPU::Register16 reg = (CPU::Register16)((params.OPCode & 0b00110000)>>4);
    uint16_t sp = params.Cpu->Get16bitRegisterValue(CPU::Register16::SP);
    //Convert to AF for this specific opcode
    if (reg == CPU::Register16::SP)
        reg = CPU::Register16::AF;
    uint16_t val = params.Cpu->Get16bitRegisterValue(reg);
    params.Cpu->DMA(sp-1, (uint8_t)(val>>8));
    params.Cpu->DMA(sp-2, (uint8_t)(val));
    params.Cpu->Set16BitRegister(CPU::Register16::SP, sp - 2);

}
