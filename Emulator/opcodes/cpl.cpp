#include "../includes.h"

CPL::CPL()
{

}

void CPL::Execute(OPCodeParameters params)
{
    uint8_t a = (params.Cpu->Get8BitRegister(CPU::Register8::A)^0xFF);
    params.Cpu->Set8BitRegister(CPU::Register8::A, a);
    params.Cpu->SetFlag(CPU::Flag::F_H, true);
    params.Cpu->SetFlag(CPU::Flag::F_N, true);
}
