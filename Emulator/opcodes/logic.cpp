#include "../includes.h"

LOGIC::LOGIC()
{

}

void LOGIC::Execute(OPCodeParameters params){
    CPU::Register8 source = ConvertTo8BitRegister(params.OPCode & 0b00000111);
    uint8_t operation = ((params.OPCode & 0b00011000)>>3);
    uint8_t val = 0x0;
    params.Cpu->SetFlag(CPU::Flag::F_H, false);
    params.Cpu->SetFlag(CPU::Flag::F_C, false);
    params.Cpu->SetFlag(CPU::Flag::F_N, false);
    switch (operation)
    {
    case 0b00:
        val = params.Cpu->Get8BitRegister(CPU::Register8::A) & params.Cpu->Get8BitRegister(source);
        params.Cpu->SetFlag(CPU::Flag::F_H, true);
        break;
    case 0b01:
        val = params.Cpu->Get8BitRegister(CPU::Register8::A) ^ params.Cpu->Get8BitRegister(source);
        break;
    case 0b10:
        val = params.Cpu->Get8BitRegister(CPU::Register8::A) | params.Cpu->Get8BitRegister(source);
        break;
    case 0b11:
        uint8_t sourceval = params.Cpu->Get8BitRegister(CPU::Register8::A);
        uint8_t o2 = params.Cpu->Get8BitRegister(ConvertTo8BitRegister(source));
        params.Cpu->SetFlag(CPU::Flag::F_C, sourceval < o2);
        params.Cpu->SetFlag(CPU::Flag::F_H, (((sourceval & 0xF) - (o2&0xF)) & 0x80) == 0x80);
        sourceval -= o2;
        params.Cpu->SetFlag(CPU::Flag::F_N, true);
        val = sourceval;
        break;
    }
    params.Cpu->SetFlag(CPU::Flag::F_Z, val == 0);
    if (operation != 0b11)
        params.Cpu->Set8BitRegister(CPU::Register8::A, val);
}
