#ifndef LDAX_H
#define LDAX_H
#include "../includes.h"

class LDAX : public OPCode
{
public:
    LDAX();
    void Execute(OPCodeParameters params);
    uint8_t numberAdditionalBytes(){return 0;}
    uint8_t formatOpcode(){return 0b11100111;}
    uint8_t checkOpcode(){return  0b00000010;}
    std::string name(){return "LDAX";}

};

#endif // LDAX_H
