#include "../includes.h"

RETI::RETI()
{

}

void RETI::Execute(OPCodeParameters params)
{
    params.Cpu->InterruptMasterEnable = true;
    RET::Execute(params);
}
