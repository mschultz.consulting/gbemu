#include "../includes.h"

ADDSUBIMMEDIATE::ADDSUBIMMEDIATE()
{

}

void ADDSUBIMMEDIATE::Execute(OPCodeParameters params)
{
    uint8_t carry = params.Cpu->GetFlag(CPU::Flag::F_C);
    params.Cpu->SetFlag(CPU::Flag::F_N, false);
    uint16_t source = params.Cpu->Get8BitRegister(CPU::Register8::A);
    if (( params.OPCode & 0b00001000) ==0)
        carry = 0;
    if ((params.OPCode & 0b00010000)!= 0)
    {
        params.Cpu->SetFlag(CPU::Flag::F_H, ((params.LowByte&0xF)+carry) > (source&0xF));
        source -= params.LowByte;
        source -= carry;
        params.Cpu->SetFlag(CPU::Flag::F_N, true);
    }
    else
    {
        params.Cpu->SetFlag(CPU::Flag::F_H, (source & 0xF) + (params.LowByte  & 0xF) + carry > 0xF);
       source += params.LowByte;
       source += carry;
    }

    params.Cpu->SetFlag(CPU::Flag::F_Z, ((source & 0x00FF) == 0));
    params.Cpu->SetFlag(CPU::Flag::F_C, ((source & 0x0100) != 0)|((source & 0x8000) != 0));
    params.Cpu->Set8BitRegister(CPU::Register8::A, (source&0x00FF));

}

