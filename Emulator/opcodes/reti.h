#ifndef RETI_H
#define RETI_H
#include "../includes.h"

class RETI : public RET
{
public:
    RETI();
    uint8_t numberAdditionalBytes(){return 0;}
    uint8_t formatOpcode(){return 0b11111111;}
    uint8_t checkOpcode(){return 0xD9;}
    void Execute(OPCodeParameters params);
    std::string name(){return "RETI";}
};

#endif // RETI_H
