#include "../includes.h"

LXI::LXI()
{

}

void LXI::Execute(OPCodeParameters params){
    uint16_t data = params.HighByte<<8;
    data +=params.LowByte;
    params.Cpu->Set16BitRegister(ConvertTo16BitRegister((params.OPCode & 0b00110000)>>4), data);
}
