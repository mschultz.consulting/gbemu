#ifndef ADDHL_H
#define ADDHL_H
#include "../includes.h"

class ADDHL : public OPCode
{
public:
    ADDHL();
    uint8_t numberAdditionalBytes(){return 0;}
    uint8_t formatOpcode(){return 0b11001111;}
    uint8_t checkOpcode() {return 0b00001001;}
    std::string name(){return "ADDHL";}
    void Execute(OPCodeParameters params);
};

#endif // ADDHL_H
