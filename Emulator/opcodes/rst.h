#ifndef RST_H
#define RST_H
#include "../includes.h"

class RST : public CALL
{
public:
    RST();
    uint8_t numberAdditionalBytes(){return 0;}
    uint8_t formatOpcode(){return 0b11000111;}
    uint8_t checkOpcode(){return 0b11000111;}
    void Execute(OPCodeParameters params);
};

#endif // RST_H
