#ifndef JRI_H
#define JRI_H
#include "../includes.h"

class JRI : public OPCode, public JMPOP
{
public:
    JRI();
    uint8_t numberAdditionalBytes(){return 1;}
    uint8_t formatOpcode(){return 0xFF;}
    uint8_t checkOpcode(){return 0x18;}
    std::string name(){return "JRI";}
    void Execute(OPCodeParameters params);
};

#endif // JRI_H
