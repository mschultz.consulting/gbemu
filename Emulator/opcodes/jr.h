#ifndef JR_H
#define JR_H
#include "../includes.h"

class JR :public JMPOP, public OPCode
{
public:
    JR();
    void Execute(OPCodeParameters params);
    uint8_t numberAdditionalBytes() { return 1;}
    uint8_t formatOpcode() { return 0b11100111;}
    uint8_t checkOpcode(){return    0b00100000;}
    std::string name(){return "JR";}
private:

};

#endif // JR_H
