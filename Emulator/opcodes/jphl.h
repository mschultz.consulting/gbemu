#ifndef JPHL_H
#define JPHL_H
#include "../includes.h"

class JPHL : public OPCode
{
public:
    JPHL();
    uint8_t numberAdditionalBytes(){return 0;}
    uint8_t formatOpcode(){return 0xFF;}
    uint8_t checkOpcode() {return 0xE9;}
    std::string name(){return "JPHL";}
    void Execute(OPCodeParameters params);
};

#endif // JPHL_H
