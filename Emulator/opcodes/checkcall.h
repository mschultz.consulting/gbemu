#ifndef CHECKCALL_H
#define CHECKCALL_H
#include "../includes.h"

class CheckCALL : public CALL, public JMPOP
{
public:
    CheckCALL();
    uint8_t numberAdditionalBytes(){return 2;}
    void Execute(OPCodeParameters params);
    uint8_t formatOpcode(){return 0b11100111;}
    uint8_t checkOpcode(){return 0b11000100;}
    std::string name(){return "CheckCALL";}
    virtual ~CheckCALL(){}
};

#endif // CHECKCALL_H
