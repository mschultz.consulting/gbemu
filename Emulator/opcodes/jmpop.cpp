#include "../includes.h"

JMPOP::JMPOP()
{
}

JMPOP::JMPType JMPOP::DetermineJMPType(uint8_t bitflag)
{
    return (JMPType) (bitflag & 0b00000011);
}

bool JMPOP::ShouldJump(JMPType type, CPU * cpu)
{
    if (type == JMPType::NZ)
        return !cpu->GetFlag(CPU::Flag::F_Z);
    if (type == JMPType::Z)
        return cpu->GetFlag(CPU::Flag::F_Z);
    if (type == JMPType::NC)
        return !cpu->GetFlag(CPU::Flag::F_C);
    if (type == JMPType::C)
        return cpu->GetFlag(CPU::Flag::F_C);
    return false;
}
