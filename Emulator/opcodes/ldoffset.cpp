#include "../includes.h"

LDOffset::LDOffset()
{

}

void LDOffset::Execute(OPCodeParameters params)
{
    signed char offset = 1;
    if ((params.OPCode&0b00010000) != 0){
        offset = -1;
    }
    CPU::Register8 src = CPU::Register8::A, dest = CPU::Register8::M;
    if ((params.OPCode&0b00001000) != 0)
    {
        src = CPU::Register8::M;
        dest = CPU::Register8::A;
    }
    params.Cpu->Set8BitRegister(dest, params.Cpu->Get8BitRegister(src));
    params.Cpu->AddTo16BitRegister(CPU::Register16::HL, offset);
}
