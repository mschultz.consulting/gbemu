#include "../includes.h"

RST::RST()
{

}

void RST::Execute(OPCodeParameters params){
     params.LowByte = ((params.OPCode>>3)&0b111)*8;
    params.HighByte = 0x0;
    CALL::Execute(params);
}
