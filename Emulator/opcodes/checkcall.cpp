#include "../includes.h"

CheckCALL::CheckCALL()
{

}

void CheckCALL::Execute(OPCodeParameters params)
{

    JMPType jmp = DetermineJMPType(params.OPCode>>3);
    if (ShouldJump(jmp, params.Cpu))
        CALL::Execute(params);
}
