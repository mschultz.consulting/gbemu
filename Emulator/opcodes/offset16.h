#ifndef OFFSET16_H
#define OFFSET16_H
#include "../includes.h"

class Offset16 : public OPCode
{
public:
    Offset16();
    uint8_t numberAdditionalBytes(){return 0;}
    uint8_t formatOpcode(){return 0b11000111;}
    uint8_t checkOpcode(){return 0b00000011;}
    std::string name(){return "Offset 16 bit reg";}
    void Execute(OPCodeParameters params);

};

#endif // OFFSET16_H
