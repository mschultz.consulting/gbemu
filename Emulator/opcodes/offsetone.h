#ifndef OFFSETONE_H
#define OFFSETONE_H
#include "../includes.h"

class OffsetOne : public OPCode
{
public:
    OffsetOne();
    void Execute(OPCodeParameters params);
    uint8_t formatOpcode(){return 0b11000110 ; }
    uint8_t checkOpcode(){return 0b00000100; }
    uint8_t numberAdditionalBytes(){return 0;}
    std::string name(){return "INC/DEC";}
};

#endif // OFFSETONE_H
