#include "../includes.h"

CheckRET::CheckRET()
{

}

void CheckRET::Execute(OPCodeParameters params)
{
    JMPType jmp = DetermineJMPType(params.OPCode>>3);
    if (ShouldJump(jmp, params.Cpu))
        RET::Execute(params);
}
