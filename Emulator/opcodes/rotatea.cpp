#include "../includes.h"

RotateA::RotateA()
{

}

void RotateA::Execute(OPCodeParameters params)
{
    params.LowByte = params.OPCode;
    BitManipOP::Execute(params);
    params.Cpu->SetFlag(CPU::Flag::F_Z, false);
}
