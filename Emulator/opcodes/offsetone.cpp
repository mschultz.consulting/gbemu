#include "../includes.h"

OffsetOne::OffsetOne()
{

}

void OffsetOne::Execute(OPCodeParameters params)
{
    CPU::Register8 source = ConvertTo8BitRegister((params.OPCode&0b00111000)>>3);
    char offset = 0;
    if((params.OPCode&0b00000001) == 0)
        offset = 1;
    else
        offset = -1;
    uint8_t val =  params.Cpu->Get8BitRegister(source);

    if ((val&0xF) == 0xF && offset == 1)
        params.Cpu->SetFlag(CPU::Flag::F_H, true);
    else if ((val & 0xF) == 0x0 && offset == -1)
        params.Cpu->SetFlag(CPU::Flag::F_H, true);
    else
        params.Cpu->SetFlag(CPU::Flag::F_H, false);

    val += offset;
    params.Cpu->SetFlag(CPU::Flag::F_Z, val == 0);
    params.Cpu->SetFlag(CPU::Flag::F_N, offset == -1);


    params.Cpu->Set8BitRegister(source,val);
}
