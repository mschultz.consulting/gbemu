#ifndef LDHLSPI_H
#define LDHLSPI_H
#include "../includes.h"

class LDHLSPI : public OPCode
{
public:
    LDHLSPI();

    uint8_t numberAdditionalBytes(){return 1;}
    uint8_t formatOpcode(){return 0xFF;}
    uint8_t checkOpcode() {return 0xF8;}
    std::string name(){return "LDHLSPI";}
    void Execute(OPCodeParameters params);
};

#endif // LDHLSPI_H
