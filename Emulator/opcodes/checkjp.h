#ifndef CHECKJP_H
#define CHECKJP_H
#include "../includes.h"

class CheckJP : public JP, JMPOP
{
public:
    CheckJP();
    uint8_t numberAdditionalBytes(){return 2;}
    uint8_t formatOpcode(){return 0b11100111;}
    uint8_t checkOpcode(){return  0b11000010;}
    void Execute(OPCodeParameters params);
    std::string name(){return "CheckJP";}
};

#endif // CHECKJP_H
