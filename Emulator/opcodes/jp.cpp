#include "../includes.h"

JP::JP()
{

}

void JP::Execute(OPCodeParameters params)
{
    uint16_t memLoc = params.HighByte;
    memLoc = memLoc<<8;
    memLoc += params.LowByte;
    params.Cpu->Set16BitRegister(CPU::Register16::PC, memLoc);
}
