#ifndef ADDSUB_H
#define ADDSUB_H
#include "../includes.h"

class ADDSUB : public OPCode
{
public:
    ADDSUB();
    uint8_t numberAdditionalBytes(){return 0;}
    uint8_t formatOpcode(){return 0b11100000;}
    uint8_t checkOpcode(){return 0b10000000;}
    void Execute(OPCodeParameters params);
    std::string name(){return "ADD/SUB";}
};

#endif // ADDSUB_H
