#ifndef JP_H
#define JP_H
#include "../includes.h"
class JP : public OPCode
{
public:
    JP();
    uint8_t numberAdditionalBytes(){return 2;}
    uint8_t formatOpcode(){return 0xFF;}
    uint8_t checkOpcode(){return 0xC3;}
    void Execute(OPCodeParameters params);
    std::string name(){return "JP";}
};

#endif // JP_H
