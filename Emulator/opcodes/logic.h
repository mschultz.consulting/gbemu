#ifndef LOGIC_H
#define LOGIC_H
#include "../includes.h"


class LOGIC : public OPCode
{
public:
    LOGIC();
    uint8_t numberAdditionalBytes() {return 0;}
    void Execute(OPCodeParameters params);
    uint8_t formatOpcode(){return 0b11100000;}
    uint8_t checkOpcode(){return 0b10100000;}
    std::string name(){return "LOGIC";}
};

#endif // LOGIC_H
