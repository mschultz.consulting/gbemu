#include "../includes.h"

LDIOC::LDIOC()
{

}

void LDIOC::Execute(OPCodeParameters params){
    uint16_t address = 0xFF00 + params.Cpu->Get8BitRegister(CPU::Register8::C);

    if ((params.OPCode & 0b00010000) == 0){
        params.Cpu->DMA(address, params.Cpu->Get8BitRegister(CPU::Register8::A));
    }
    else{
        params.Cpu->Set8BitRegister(CPU::Register8::A, params.Cpu->DMA(address));
    }
}
