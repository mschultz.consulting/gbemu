#include "../includes.h"

LogicImmediate::LogicImmediate()
{

}

void LogicImmediate::Execute(OPCodeParameters params){
    uint8_t operation = ((params.OPCode & 0b00011000)>>3);
    uint8_t val = 0x0;
    params.Cpu->SetFlag(CPU::Flag::F_H, false);
    params.Cpu->SetFlag(CPU::Flag::F_C, false);
    params.Cpu->SetFlag(CPU::Flag::F_N, false);
    switch (operation)
    {
    case 0b00:
        val = params.Cpu->Get8BitRegister(CPU::Register8::A) &params.LowByte;
        params.Cpu->SetFlag(CPU::Flag::F_H, true);
        break;
    case 0b01:
        val = params.Cpu->Get8BitRegister(CPU::Register8::A) ^params.LowByte;
        break;
    case 0b10:
        val = params.Cpu->Get8BitRegister(CPU::Register8::A) |params.LowByte;
        break;
    case 0b11:
        uint16_t sourceval = params.Cpu->Get8BitRegister(CPU::Register8::A);
        params.Cpu->SetFlag(CPU::Flag::F_C, (sourceval < params.LowByte));
        params.Cpu->SetFlag(CPU::Flag::F_H, (((sourceval & 0xF) - (params.LowByte&0xF)) & 0x80) == 0x80);
        sourceval -=params.LowByte;
        params.Cpu->SetFlag(CPU::Flag::F_N, true);
        val = (sourceval & 0x00FF);
        break;
    }
    params.Cpu->SetFlag(CPU::Flag::F_Z, val == 0);
    if (operation != 0b11)
        params.Cpu->Set8BitRegister(CPU::Register8::A, val);
}
