#ifndef LDD_H
#define LDD_H
#include "../includes.h"

class LDOffset : public OPCode
{
public:
    LDOffset();
    void Execute(OPCodeParameters params);
    uint8_t numberAdditionalBytes() { return 0;}
    uint8_t formatOpcode(){return 0b11100111;}
    uint8_t checkOpcode(){return 0b00100010;}
    std::string name(){return "LDOffset";}
};

#endif // LDD_H
