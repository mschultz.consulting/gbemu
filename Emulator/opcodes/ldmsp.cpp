#include "../includes.h"

LDMSP::LDMSP()
{

}


void LDMSP::Execute(OPCodeParameters params)
{
    uint16_t addr = ((uint16_t)params.HighByte<<8) + params.LowByte;
    uint16_t sp =  params.Cpu->Get16bitRegisterValue(CPU::Register16::SP);
    params.Cpu->DMA(addr, sp&0xFF );
    params.Cpu->DMA(addr-1, ((sp&0xFF00)>>8) );
}
