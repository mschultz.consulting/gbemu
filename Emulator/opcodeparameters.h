#ifndef OPCODEPARAMETERS_H
#define OPCODEPARAMETERS_H
#include "includes.h"


class OPCodeParameters
{
public:
    OPCodeParameters(CPU* cpu);
    uint8_t OPCode;
    uint8_t HighByte;
    uint8_t LowByte;
    CPU* Cpu;
};

#endif // OPCODEPARAMETERS_H
