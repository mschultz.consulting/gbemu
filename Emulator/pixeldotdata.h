#ifndef PIXELDOTDATA_H
#define PIXELDOTDATA_H
#include "includes.h"

class PixelDotData
{
private:
    uint8_t highData, midHighData,midLowData,lowData;
    const uint8_t PX_COLOR_BLACK = 0xFF;
    const uint8_t PX_HIGH_BIT_SET = 170;
    const uint8_t PX_LOW_BIT_SET = 85;

    uint8_t addToRegister(uint8_t dataValue, Bit high, Bit low);
    void SetData(uint8_t reg, uint8_t* data, Bit high, Bit low);
public:
    enum Map{
        High = 0b11,
        MidHigh = 0b10,
        MidLow = 0b01,
        Low = 0b00,
    };

    PixelDotData();
    void SetRegister(uint8_t data);
    uint8_t GetRegister();
    uint8_t GetShade(Map map);
};

#endif // PIXELDOTDATA_H
