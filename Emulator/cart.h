#ifndef CART_H
#define CART_H
#include "includes.h"

class Cart
{

public:
    enum MemoryMapType{
        ROMOnly,
        MBC1,
        MBC2,
        MBC3,
        MBC5,
        MMO1
    };

    Cart(uint8_t* data, uint64_t size);
    Cart(const Cart&);
    Cart& operator=(const Cart&);
    std::string GetName(){return name;}
    bool HasRam(){return hasRam;}
    bool HasBatt(){return hasBatt;}
    bool HasTimer(){return hasTimer;}
    MemoryMapType GetMemoryMapType(){return mmt;}
    uint64_t size(){return cartSize;}
    uint8_t* getCartData(){return cartData;}
private:
    uint8_t* cartData;
    uint64_t cartSize;
    bool hasRam;
    bool hasBatt;
    bool hasTimer;
    MemoryMapType mmt;
    std::string name;
};

#endif // CART_H
