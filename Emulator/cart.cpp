#include "includes.h"

Cart::Cart(uint8_t* data, uint64_t size)
    : cartData(data)
    , cartSize(size)
    , hasRam(false)
    , hasBatt(false)
    , hasTimer(false)
    , mmt(MemoryMapType::ROMOnly)
    , name("BIOS")
{
    if (size > 0x14f)//Verify that this is a real cart
    {
        name = std::string((char*)(data+0x134), (0x142-0x134));
        uint8_t cartType = data[147];
        switch(cartType)
        {
        case 0:
        break;
            //Purposefully fall through
        case 0x3:
            hasBatt = true;
        case 0x2:
            hasRam = true;
        case 0x1:
            mmt = MemoryMapType::MBC1;
            break;
        case 0x6:
            hasBatt = true;
        case 0x5:
            mmt = MemoryMapType::MBC2;
            break;
        case 0x9:
            hasBatt = true;
        case 0x8:
            hasRam = true;
            break;
        case 0xD:
            hasBatt = true;
        case 0xC:
            hasRam = true;
        case 0xB:
            mmt = MemoryMapType::MMO1;
            break;
        case 0x1E:
        case 0x1B:
            hasBatt = true;
        case 0x1D:
        case 0x1A:
            hasRam = true;
        case 0x1C:
        case 0x19:
            mmt = MemoryMapType::MBC5;
            break;

        }
    }
}
