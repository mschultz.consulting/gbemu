#include "includes.h"
#include <allegro5/allegro.h>
#include <allegro5/allegro5.h>

GameBoy::GameBoy(uint8_t *bios, uint64_t size, uint8_t *cartData, uint64_t cartSize)
    : bus(new Bus())
{
    if (!al_init() || !al_init_primitives_addon())
    {
        std::cout<<"FAILED TO INIT ALLEGRO";
        exit(-1);
    }

    ALLEGRO_DISPLAY* display = al_create_display(GPU::SCREEN_SIZE_X,GPU::SCREEN_SIZE_Y);

    if (display == NULL)
    {
        std::cout<<"FAILED TO INIT ALLEGRO_DISPLAY";
        exit(-1);
    }
    if (!al_install_keyboard())
    {
        std::cout<<"Failed to init keyboard";
        exit(-1);
    }

    CPU* cpu = new CPU(bus);

    GPU* gpu = new GPU(bus, NULL);
    gpu->SetScreenMultiplier(1, display);
    Cart* biosCart = new Cart(bios, size);
    Cart* gbCart = new Cart(cartData, cartSize);
    Input* buttons = new Input(bus);
    SoundCard* sound = new SoundCard();
    switch (gbCart->GetMemoryMapType())
    {
    case Cart::MemoryMapType::MBC3:
        bus->Map = new MBC3(bus);
        break;
    default:
        bus->Map = new MemoryMap(bus);
    }

    bus->Cpu = cpu;
    bus->Gpu = gpu;
    bus->BIOS = biosCart;
    bus->CartData = gbCart;
    bus->Buttons = buttons;
    bus->Sound = sound;
}

void GameBoy::FrameLimiter(float limitMultiplier)
{
    if (std::chrono::duration_cast<std::chrono::microseconds>(Clock::now() - oldClock).count() > 1)
    {
        unsigned long long cyclesCompleted = bus->Cpu->Cycles - oldCycles;
        float ratio = cyclesCompleted/(8.0f * limitMultiplier);
        while(std::chrono::duration_cast<std::chrono::microseconds>(Clock::now() - oldClock).count() < ratio);
        oldCycles = bus->Cpu->Cycles;
        oldClock = Clock::now();
    }
}

void GameBoy::Tick()
{
    bus->Cpu->Tick();
    bus->Gpu->Tick();
    bus->Buttons->Tick();
    //FrameLimiter(1);
}
