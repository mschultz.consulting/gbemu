#ifndef INPUT_H
#define INPUT_H
#include "includes.h"
#include "allegro5/allegro.h"
class Input
{

public:
    enum Button{
        Up,
        Down,
        Left,
        Right,
        A,
        B,
        Start,
        Select
    };

    Input(Bus*);
    Input(const Input&);
    bool operator=(const Input&);
    void WriteRegister(uint8_t value);
    uint8_t ReadRegister();
    void Tick();

    ~Input();
private:
    //Describe the two pins for allowing input
    bool p14Set, p15Set;

    //Describe the other 4 pins for input set
    bool p10Set, p11Set, p12Set, p13Set;
    void SetButton(Button value);
    Bus* bus;
    std::map<Button, int> map = {
        {Button::Up, ALLEGRO_KEY_UP},
        {Button::Down, ALLEGRO_KEY_DOWN},
        {Button::Left, ALLEGRO_KEY_LEFT},
        {Button::Right, ALLEGRO_KEY_RIGHT},
        {Button::A, ALLEGRO_KEY_A},
        {Button::B, ALLEGRO_KEY_S},
        {Button::Start, ALLEGRO_KEY_ENTER},
        {Button::Select, ALLEGRO_KEY_RSHIFT},
    };
};

#endif // INPUT_H

