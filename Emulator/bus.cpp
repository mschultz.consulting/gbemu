#include "bus.h"

Bus::Bus()
    : BIOS(NULL)
    , Buttons(NULL)
    , CartData(NULL)
    , Cpu(NULL)
    , Gpu(NULL)
    , Map(NULL)
    , Sound(NULL)
{
    //Initialize to empty bus
}

Bus::~Bus()
{
}
Bus::Bus(const Bus& copy)
    :BIOS (copy.BIOS)
    , Buttons (copy.Buttons)
    , CartData (copy.CartData)
    , Cpu (copy.Cpu)
    , Gpu (copy.Gpu)
    , Map (copy.Map)
    , Sound (copy.Sound)
{
}

Bus& Bus::operator=(const Bus& copy)
{
    this->BIOS = copy.BIOS;
    this->Buttons = copy.Buttons;
    this->CartData = copy.CartData;
    this->Cpu = copy.Cpu;
    this->Gpu = copy.Gpu;
    this->Map = copy.Map;
    this->Sound = copy.Sound;
    return *this;
}
