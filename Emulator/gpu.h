#ifndef GPU_H
#define GPU_H
#include "includes.h"
#include "allegro5/allegro.h"
#include <allegro5/allegro_primitives.h>

class GPU
{
public:

    static const uint8_t SCREEN_SIZE_X = 160;
    static const uint8_t SCREEN_SIZE_Y = 144;

    GPU(Bus* bus, ALLEGRO_DISPLAY* display);
    GPU(const GPU&);
    GPU& operator=(const GPU&);
    void Tick();
    ~GPU();
    uint8_t Scanline();
    void ResetScanline();
    uint8_t ScrollX;
    uint8_t ScrollY;
    uint8_t ScanlineCompare;
    bool ScanlineCompareInterruptEnabled;
    bool OAMInterruptEnabled;
    bool VBlankInterruptEnabled;
    bool HBlankInterruptEnabled;

    uint8_t GetBackgroundPaletteData();
    void SetBackgroundPaletteData(uint8_t val);
    uint8_t GetObject0PaletteData();
    void SetObject0PaletteData(uint8_t val);
    uint8_t GetObject1PaletteData();
    void SetObject1PaletteData(uint8_t val);
    uint8_t GetLCDC();
    void SetLCDC(uint8_t value);
    uint8_t GetWindowLocationX();
    void SetWindowLocationX(uint8_t);
    uint8_t GetWindowLocationY();
    void SetWindowLocationY(uint8_t);
    void DrawScanline();
    uint8_t GetLCDCStatus();
    void SetLCDCStatus(uint8_t val);
    void SetScreenMultiplier(uint8_t, ALLEGRO_DISPLAY* disp);

private:
    enum GPUMode{
        HBlank = 0b00,
        VBlank = 0b01,
        Processing = 0b10,
        Writing = 0b11
    };

    const uint16_t VRAM_START = 0x8000;
    const uint8_t VBLANK_START = 0x90;



    //Initializers
    const uint8_t LCDC_INIT = 0x91;
    const uint8_t WINDOW_LOCATION_X_INIT = 0;
    const uint8_t WINDOW_LOCATION_Y_INIT = 0;

    //LCDC values
    const uint16_t TILE_MAP_DISPLAY_SELECT_0 = 0x9800;
    const uint16_t TILE_MAP_DISPLAY_SELECT_1 = 0x9C00;
    const uint16_t TILE_MAP_DISPLAY_SIZE = 0x3FF;

    const uint16_t BACKGROUND_AND_WINDOW_TILE_DATA_SELECT_0 = 0x8800;
    const uint16_t BACKGROUND_AND_WINDOW_TILE_DATA_SELECT_1 = 0x8000;
    const uint16_t BACKGROUND_AND_WINDOW_TILE_DATA_SIZE = 0xFFF;

    const uint8_t SPRITE_SIZE_0 = 8*8;
    const uint8_t SPRITE_SIZE_1 = 8*16;

    const uint16_t H_BLANK_CYCLES = 204;
    const uint16_t PROCESSING_CYCLES = 80 + H_BLANK_CYCLES;
    const uint16_t WRITING_CYCLES = 172 + PROCESSING_CYCLES;

    uint8_t windowLocationX, windowLocationY;
    PixelDotData* backgroundPaletteData;
    PixelDotData* object0PaletteData;
    PixelDotData* object1PaletteData;

    ALLEGRO_DISPLAY *display;
    Bus *bus;

    uint8_t scanline;
    bool enabled;
    uint16_t windowTileMapDisplayLocation;
    bool windowEnabled;
    uint16_t bgAndWindowTileDataSelectLocation;
    uint16_t bgTileMapDisplaySelectLocation;
    uint8_t spriteSize;
    uint8_t screenMultiplier;

    bool displaySprites;
    bool bgAndWindowDisplayEnabled;

    uint8_t* pixels;
    ALLEGRO_VERTEX* al_pixels;

    unsigned long long cyclesAtLastScanlineWrite = 0;
    GPUMode currentMode;

    void WriteToScreen();
};

#endif // GPU_H
