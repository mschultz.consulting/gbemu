#ifndef MEMORYMAP_H
#define MEMORYMAP_H
#include "includes.h"
class Bus;

class MemoryMap
{
public:
    MemoryMap(Bus* val);
    virtual void SetMemory(uint16_t address, uint8_t data);
    virtual uint8_t GetMemory(uint16_t address);
    void PrintFullMemory();
    MemoryMap(const MemoryMap&);
    MemoryMap& operator=(const MemoryMap&);
    virtual ~MemoryMap();
    bool readFromBios;
protected:
    MemoryMap(Bus *val, bool allocate);

    Bus* bus;

    virtual void SetROMBank0(uint16_t address, uint8_t value);
    virtual void SetSwitchableROMBank(uint16_t address, uint8_t value);
    virtual void SetVRAM(uint16_t address, uint8_t value);
    virtual void SetSwitchableRAMBank(uint16_t address, uint8_t value);
    virtual void SetInternalRAM(uint16_t address, uint8_t value);
    virtual void SetEchoInternalRAM(uint16_t address, uint8_t value);
    virtual void SetSpriteAttributeMemory(uint16_t address, uint8_t value);
    virtual void SetFirstEmptyAddresses(uint16_t address, uint8_t value);
    virtual void SetIOPorts(uint16_t address, uint8_t value);
    virtual void SetSecondEmptyAddresses(uint16_t address, uint8_t value);
    virtual void SetSecondInternalRam(uint16_t address, uint8_t value);
    virtual void SetInterruptRegister(uint8_t value);
    virtual uint8_t GetROMBank0(uint16_t address);
    virtual uint8_t GetSwitchableROMBank(uint16_t address);
    virtual uint8_t GetVRAM(uint16_t address);
    virtual uint8_t GetSwitchableRAMBank(uint16_t address);
    virtual uint8_t GetInternalRAM(uint16_t address);
    virtual uint8_t GetEchoInternalRAM(uint16_t address);
    virtual uint8_t GetSpriteAttributeMemory(uint16_t address);
    virtual uint8_t GetFirstEmptyAddresses(uint16_t address);
    virtual uint8_t GetIOPorts(uint16_t address);
    virtual uint8_t GetSecondEmptyAddresses(uint16_t address);
    virtual uint8_t GetSecondInternalRam(uint16_t address);
    virtual uint8_t GetInterruptRegister();

    uint8_t* VRAM;
    uint8_t* SwitchableRAMBank;
    uint8_t* InternalRAM;
    uint8_t* SpriteAttributeMemory;
    uint8_t* SecondInternalRam;
    uint8_t  InterruptRegister;

    static const uint16_t RAM_START = 0x0;
    static const uint16_t ROM_BANK_0_MAX_ADDR = 0x4000;
    static const uint16_t SWITCHABLE_ROM_BANK_MAX_ADDR = 0x8000;
    static const uint16_t VRAM_MAX_ADDR = 0xA000;
    static const uint16_t SWITCHABLE_RAM_BANK_MAX_ADDR = 0xC000;
    static const uint16_t INTERNAL_RAM_MAX_ADDR = 0xE000;
    static const uint16_t ECHO_INTERNAL_RAM_MAX_ADDR = 0xFE00;
    static const uint16_t SPRITE_ATTRIBUTE_MEMORY_MAX_ADDR= 0xFEA0;
    static const uint16_t FIRST_EMPTY_MAX_ADDR = 0xFF00;
    static const uint16_t IO_PORTS_MAX_ADDR = 0xFF4C;
    static const uint16_t SECOND_EMPTY_MAX_ADDR = 0xFF80;
    static const uint16_t SECOND_INTERNAL_RAM_MAX_ADDR = 0xFFFF;
    static const uint16_t INTERRUPT_REGISTER_ADDR = 0xFFFF;
    static const uint8_t INVALID_MEMORY_RESPONSE = 0xFF;
private:
    void PrintMemory(uint8_t* ptr, int len);
};

#endif // MEMORYMAP_H
