cmake_minimum_required(VERSION 3.12)
project("GBEMU")
INCLUDE_DIRECTORIES( /usr/local/allegro/include )

LINK_DIRECTORIES( /usr/local/allegro/lib )



file(GLOB_RECURSE GBEMUH "Emulator/*.h")
file(GLOB_RECURSE GBEMUSRC "Emulator/*.cpp" )
file(GLOB_RECURSE TSTH "UnitTests/*.h")
file(GLOB_RECURSE TSTSRC "UnitTests/*.cpp" )
file(GLOB_RECURSE REMOVE_CMAKE "CMakeFiles/*")
set(TST ${GBEMUSRC})
list(REMOVE_ITEM TST ${CMAKE_CURRENT_SOURCE_DIR}/Emulator/main.cpp)
list(APPEND TST ${TSTSRC})

list(REMOVE_ITEM GBEMUSRC ${REMOVE_CMAKE})

add_executable(GBEMU.tst ${GBEMUH} ${TSTH} ${TST})
add_executable(GBEMU.out ${GBEMUH} ${GBEMUSRC})
TARGET_LINK_LIBRARIES(GBEMU.tst allegro )
TARGET_LINK_LIBRARIES(GBEMU.tst allegro_image )
TARGET_LINK_LIBRARIES(GBEMU.tst allegro_color )
TARGET_LINK_LIBRARIES(GBEMU.tst allegro_font )
TARGET_LINK_LIBRARIES(GBEMU.tst allegro_primitives )
TARGET_LINK_LIBRARIES(GBEMU.out allegro )
TARGET_LINK_LIBRARIES(GBEMU.out allegro_image )
TARGET_LINK_LIBRARIES(GBEMU.out allegro_color )
TARGET_LINK_LIBRARIES(GBEMU.out allegro_font )
TARGET_LINK_LIBRARIES(GBEMU.out allegro_primitives )
