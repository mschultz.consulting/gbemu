#include "tst_gbemutest.h"
#include "memorymapmock.h"
#define QCOMPARE(a, e){if (a != e) { std::cout<<"A:"<<a<<":E:"<<e<<"|"<<__FILE__<<"|"<<__LINE__<<std::endl;throw std::runtime_error("Unexpected difference");}}
#define QVERIFY2(d, s){if (!d) throw s;}

GBEMUTest::GBEMUTest()
{

}
OPCodeParameters GetParameters(CPU* cpu, uint8_t ins, uint8_t highbyte, uint8_t lowbyte )
{
    OPCodeParameters params(cpu);
    params.HighByte = highbyte;
    params.LowByte = lowbyte;
    params.OPCode = ins;
    return params;
}
OPCodeParameters GetParameters(CPU* cpu, uint8_t ins)
{
    return GetParameters(cpu, ins, 0, 0);
}

void GBEMUTest::testGPU_LCDC()
{
    initTest();

    Bus* bus = new Bus();
    bus->Map = new MemoryMapMock(bus);
    bus->Cpu = new CPU(bus);


    GPU* gpu = new GPU(bus, NULL);
    QCOMPARE(gpu->GetLCDC(), (uint8_t)0x91);
    gpu->SetLCDC(0xFF);
    QCOMPARE(gpu->GetLCDC(), (uint8_t)0xFF);
    gpu->SetLCDC(0x0);
    QCOMPARE(gpu->GetLCDC(),(uint8_t) 0b10000000);
    while(gpu->Scanline() < 0x90)
    {
        bus->Cpu->Cycles += 4;
        gpu->Tick();
    }

    gpu->SetLCDC(0x0);
    QCOMPARE(gpu->GetLCDC(),(uint8_t) 0);

    gpu->ResetScanline();
    gpu->SetLCDC(0xFF);
    QCOMPARE(gpu->GetLCDC(), (uint8_t)0b01111111);

}

void GBEMUTest::testGPU_BGP()
{
    initTest();

    Bus* bus = new Bus();
    bus->Map = new MemoryMapMock(bus);

    GPU* gpu = new GPU(bus, NULL);
    gpu->SetBackgroundPaletteData(0x0);
    QCOMPARE(gpu->GetBackgroundPaletteData(), (uint8_t)0);
    gpu->SetBackgroundPaletteData(0xFF);
    QCOMPARE(gpu->GetBackgroundPaletteData(), (uint8_t)0xFF);
}
void GBEMUTest::testGPU_OBP0()
{
    initTest();

    Bus* bus = new Bus();
    bus->Map = new MemoryMapMock(bus);

    GPU* gpu = new GPU(bus, NULL);
    gpu->SetObject0PaletteData(0x0);
    QCOMPARE(gpu->GetObject0PaletteData(), (uint8_t)0);
    gpu->SetObject0PaletteData(0xFF);
    QCOMPARE(gpu->GetObject0PaletteData(), (uint8_t)0xFF);
}
void GBEMUTest::testGPU_OBP1()
{
    initTest();

    Bus* bus = new Bus();
    bus->Map = new MemoryMapMock(bus);

    GPU* gpu = new GPU(bus, NULL);
    gpu->SetObject1PaletteData(0x0);
    QCOMPARE(gpu->GetObject1PaletteData(), (uint8_t)0);
    gpu->SetObject1PaletteData(0xFF);
    QCOMPARE(gpu->GetObject1PaletteData(), (uint8_t)0xFF);
}
void GBEMUTest::testPixelDotData()
{
    initTest();

    PixelDotData data;
    data.SetRegister(0);
    QCOMPARE(data.GetRegister(), (uint8_t)0);
    data.SetRegister(0xFF);
    QCOMPARE(data.GetRegister(), (uint8_t)0xFF);
}

void GBEMUTest::testAddSub_BasicAdd()
{
    initTest();

    CPU* cpu = new CPU(NULL);
    ADDSUB s;
    cpu->Set8BitRegister(CPU::Register8::A, 1);
    cpu->Set8BitRegister(CPU::Register8::B, 2);
    s.Execute(GetParameters(cpu, 0x80));
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::A), (uint8_t)3);
    for (int q = CPU::Register8::C; q < CPU::Register8::M; q++)
    {
        QVERIFY2(cpu->Get8BitRegister((CPU::Register8)q) == (uint8_t)0,cpu->Get8BitRegisterName((CPU::Register8)q).c_str());
    }
}

void GBEMUTest::testAddSub_ADC()
{
    initTest();

    CPU* cpu = new CPU(NULL);
    ADDSUB s;
    cpu->SetFlag(CPU::Flag::F_C, true);
    cpu->Set8BitRegister(CPU::Register8::A, 1);
    cpu->Set8BitRegister(CPU::Register8::B, 2);
    s.Execute(GetParameters(cpu, 0x88));
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::A), (uint8_t)4);
    for (int q = CPU::Register8::C; q < CPU::Register8::M; q++)
    {
        QVERIFY2(cpu->Get8BitRegister((CPU::Register8)q) == (uint8_t)0,cpu->Get8BitRegisterName((CPU::Register8)q).c_str());
    }
}
void GBEMUTest::testCP()
{
    initTest();

    CPU* cpu = new CPU(NULL);
    LOGIC s;
    cpu->Set8BitRegister(CPU::Register8::A, 1);
    cpu->Set8BitRegister(CPU::Register8::B, 1);
    s.Execute(GetParameters(cpu, 0xB8));
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), true);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::A), (uint8_t)1);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::B), (uint8_t)1);
    for (int q = CPU::Register8::C; q < CPU::Register8::M; q++)
    {
        QVERIFY2(cpu->Get8BitRegister((CPU::Register8)q) == (uint8_t)0,cpu->Get8BitRegisterName((CPU::Register8)q).c_str());
    }
    cpu->Set8BitRegister(CPU::Register8::A, 1);
    cpu->Set8BitRegister(CPU::Register8::B, 2);
    s.Execute(GetParameters(cpu, 0xB8));
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), true);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::A), (uint8_t)1);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::B), (uint8_t)2);
    for (int q = CPU::Register8::C; q < CPU::Register8::M; q++)
    {
        QVERIFY2(cpu->Get8BitRegister((CPU::Register8)q) == (uint8_t)0,cpu->Get8BitRegisterName((CPU::Register8)q).c_str());
    }
}

void GBEMUTest::testAddSub_SBC()
{
    initTest();

    CPU* cpu = new CPU(NULL);
    ADDSUB s;
    cpu->SetFlag(CPU::Flag::F_C, true);
    cpu->Set8BitRegister(CPU::Register8::A, 10);
    cpu->Set8BitRegister(CPU::Register8::B, 9);
    s.Execute(GetParameters(cpu, 0x98));
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), true);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::A), (uint8_t)0);
    for (int q = CPU::Register8::C; q < CPU::Register8::M; q++)
    {
        QVERIFY2(cpu->Get8BitRegister((CPU::Register8)q) == (uint8_t)0,cpu->Get8BitRegisterName((CPU::Register8)q).c_str());
    }
}

void GBEMUTest::testAddSub_AddWithCarryZero()
{
    initTest();

    CPU* cpu = new CPU(NULL);
    ADDSUB s;
    cpu->Set8BitRegister(CPU::Register8::A, 255);
    cpu->Set8BitRegister(CPU::Register8::B, 1);
    s.Execute(GetParameters(cpu, 0x80));
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::A), (uint8_t)0);
    for (int q = CPU::Register8::C; q < CPU::Register8::M; q++)
    {
        QVERIFY2(cpu->Get8BitRegister( (CPU::Register8)q)== (uint8_t)0, cpu->Get8BitRegisterName((CPU::Register8)q).c_str());
    }
}


void GBEMUTest::testAddSub_BasicSubtract()
{
    initTest();

    CPU* cpu = new CPU(NULL);
    ADDSUB s;
    cpu->Set8BitRegister(CPU::Register8::A, 10);
    cpu->Set8BitRegister(CPU::Register8::B, 10);
    s.Execute(GetParameters(cpu, 0x90));
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), true);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::A), (uint8_t)0);
    for (int q = CPU::Register8::C; q < CPU::Register8::M; q++)
    {
        QVERIFY2(cpu->Get8BitRegister((CPU::Register8)q) == (uint8_t)0, cpu->Get8BitRegisterName((CPU::Register8)q).c_str());
    }
}

void GBEMUTest::testAddSub_SubtractWithCarryZero()
{
    initTest();

    CPU* cpu = new CPU(NULL);
    ADDSUB s;
    cpu->Set8BitRegister(CPU::Register8::A, 5);
    cpu->Set8BitRegister(CPU::Register8::B, 10);
    s.Execute(GetParameters(cpu, 0x90));
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), true);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::A), (uint8_t)251);
    for (int q = CPU::Register8::C; q < CPU::Register8::M; q++)
    {
        QVERIFY2(cpu->Get8BitRegister((CPU::Register8)q) == (uint8_t)0,cpu->Get8BitRegisterName((CPU::Register8)q).c_str());
    }
}

void GBEMUTest::testCFOp_Set()
{
    initTest();

    CPU* cpu = new CPU(NULL);
    CFOp op;
    cpu->SetFlag(CPU::Flag::F_C,false);
    cpu->SetFlag(CPU::Flag::F_H,true);
    cpu->SetFlag(CPU::Flag::F_Z,true);
    cpu->SetFlag(CPU::Flag::F_N,true);
    op.Execute(GetParameters(cpu, 0x37));
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
    cpu->SetFlag(CPU::Flag::F_C,true);
    cpu->SetFlag(CPU::Flag::F_H,true);
    cpu->SetFlag(CPU::Flag::F_Z,true);
    cpu->SetFlag(CPU::Flag::F_N,true);
    op.Execute(GetParameters(cpu, 0x37));
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
}

void GBEMUTest::testCFOp_Comp()
{
    initTest();

    CPU* cpu = new CPU(NULL);
    CFOp op;
    cpu->SetFlag(CPU::Flag::F_C,false);
    cpu->SetFlag(CPU::Flag::F_H,true);
    cpu->SetFlag(CPU::Flag::F_Z,true);
    cpu->SetFlag(CPU::Flag::F_N,true);
    op.Execute(GetParameters(cpu, 0x3F));
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
    cpu->SetFlag(CPU::Flag::F_C,true);
    cpu->SetFlag(CPU::Flag::F_H,true);
    cpu->SetFlag(CPU::Flag::F_Z,true);
    cpu->SetFlag(CPU::Flag::F_N,true);
    op.Execute(GetParameters(cpu, 0x3F));
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
}

void GBEMUTest::testBMRES()
{
    initTest();


    CPU* cpu = new CPU(NULL);
    cpu->Set8BitRegister(CPU::Register8::B, 0);
    BitManipOP op;

    OPCodeParameters params(cpu);
    params.Cpu = cpu;
    params.OPCode = 0xCB;
    for (int i = 0x80; i < 0xBF; i+= 8)
    {
        for (int q = 0; q < CPU::Register8::M; q++)
            cpu->Set8BitRegister((CPU::Register8)q, 0x0);
        cpu->Set8BitRegister(CPU::Register8::B, 0xFF);
        params.LowByte = i;
        cpu->SetFlag(CPU::Flag::F_C, false);
        cpu->SetFlag(CPU::Flag::F_H, false);
        cpu->SetFlag(CPU::Flag::F_N, false);
        cpu->SetFlag(CPU::Flag::F_Z, false);
        op.Execute(params);
        QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), false);
        QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), false);
        QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
        QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
        uint8_t val = (0x1 << ((i-0x80)/8));
        val ^= 0xFF;
        QCOMPARE(cpu->Get8BitRegister(CPU::Register8::B), val  );
        for (int q = 0; q < CPU::Register8::M; q++)
            cpu->Set8BitRegister((CPU::Register8)q, 0xFF);
        cpu->Set8BitRegister(CPU::Register8::B, 0x0);
        cpu->SetFlag(CPU::Flag::F_C, true);
        cpu->SetFlag(CPU::Flag::F_H, true);
        cpu->SetFlag(CPU::Flag::F_N, true);
        cpu->SetFlag(CPU::Flag::F_Z, true);
        op.Execute(params);
        QCOMPARE(cpu->Get8BitRegister(CPU::Register8::B), (uint8_t)0  );
        QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), true);
        QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), true);
        QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), true);
        QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), true);
    }
}
void GBEMUTest::initTest()
{
    this->testNumber++;
    std::cout<<"Starting test: "<<std::dec<<this->testNumber<<std::endl;
}
void GBEMUTest::testBMZBit()
{
    initTest();

    CPU* cpu = new CPU(NULL);
    cpu->Set8BitRegister(CPU::Register8::B, 0);
    BitManipOP op;

    OPCodeParameters params(cpu);
    params.Cpu = cpu;
    params.OPCode = 0xCB;
    for (int i = 0x40; i < 0x7F; i+= 8)
    {
        for (int q = 0; q < CPU::Register8::M; q++)
            cpu->Set8BitRegister((CPU::Register8)q, 0x0);
        cpu->Set8BitRegister(CPU::Register8::B, 0xFF);
        params.LowByte = i;
        cpu->SetFlag(CPU::Flag::F_C, false);
        cpu->SetFlag(CPU::Flag::F_H, false);
        cpu->SetFlag(CPU::Flag::F_N, false);
        cpu->SetFlag(CPU::Flag::F_Z, false);
        op.Execute(params);
        QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), false);
        QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), false);
        QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), true);
        QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
        for (int q = 0; q < CPU::Register8::M; q++)
            cpu->Set8BitRegister((CPU::Register8)q, 0xFF);
        cpu->Set8BitRegister(CPU::Register8::B, 0x0);
        cpu->SetFlag(CPU::Flag::F_C, true);
        cpu->SetFlag(CPU::Flag::F_H, true);
        cpu->SetFlag(CPU::Flag::F_N, true);
        cpu->SetFlag(CPU::Flag::F_Z, false);
        op.Execute(params);
        QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), true);
        QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), true);
        QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), true);
        QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
    }
}
void GBEMUTest::testBMRRC()
{
    initTest();

    BitManipOP op;
    CPU* cpu = new CPU(NULL);
    //Basic
    cpu->Set8BitRegister(CPU::Register8::B, 0b10101011);
    cpu->SetFlag(CPU::Flag::F_C, false);
    cpu->SetFlag(CPU::Flag::F_H, true);
    cpu->SetFlag(CPU::Flag::F_N, true);
    cpu->SetFlag(CPU::Flag::F_Z, true);
    op.Execute00(cpu, 0x8, CPU::Register8::B);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::B),(uint8_t) 0b11010101);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
    //Zero
    cpu->Set8BitRegister(CPU::Register8::B, 0b00000001);
    cpu->SetFlag(CPU::Flag::F_C, false);
    cpu->SetFlag(CPU::Flag::F_H, true);
    cpu->SetFlag(CPU::Flag::F_N, true);
    cpu->SetFlag(CPU::Flag::F_Z, true);
    op.Execute00(cpu, 0x8, CPU::Register8::B);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::B),(uint8_t) 0x80);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
    //carry
    cpu->Set8BitRegister(CPU::Register8::B, 0b01010101);
    cpu->SetFlag(CPU::Flag::F_C, true);
    cpu->SetFlag(CPU::Flag::F_H, true);
    cpu->SetFlag(CPU::Flag::F_N, true);
    cpu->SetFlag(CPU::Flag::F_Z, true);
    op.Execute00(cpu, 0x8, CPU::Register8::B);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::B),(uint8_t) 0b10101010);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
}
void GBEMUTest::TestCALL()
{
    initTest();

    CALL op;
    Bus* bus = new Bus();
    MemoryMap* map = new MemoryMapMock(bus);
    bus->Map=map;
    CPU* cpu = new CPU(bus);
    cpu->Set16BitRegister(CPU::Register16::PC, 0xAAFF);
    cpu->Set16BitRegister(CPU::Register16::SP, 0xFFFE);
    OPCodeParameters params(cpu);
    params.HighByte = 0xBB;
    params.LowByte = 0xCC;
    op.Execute(params);
    QCOMPARE(cpu->Get16bitRegisterValue(CPU::Register16::PC), (uint16_t)0xBBCC);
    QCOMPARE(cpu->Get16bitRegisterValue(CPU::Register16::SP),(uint16_t) 0xFFFC);
    QCOMPARE(map->GetMemory(0xFFFD), (uint8_t)0xAA);
    QCOMPARE(map->GetMemory(0xFFFC), (uint8_t)0xFF);
    RET r;
    r.Execute(params);
    QCOMPARE(cpu->Get16bitRegisterValue(CPU::Register16::PC), (uint16_t)0xAAFF);
    QCOMPARE(cpu->Get16bitRegisterValue(CPU::Register16::SP),(uint16_t) 0xFFFE);
}

void GBEMUTest::testBMRR()
{
    initTest();

    BitManipOP op;
    CPU* cpu = new CPU(NULL);
    //Basic
    cpu->Set8BitRegister(CPU::Register8::B, 0b10101010);
    cpu->SetFlag(CPU::Flag::F_C, true);
    cpu->SetFlag(CPU::Flag::F_H, true);
    cpu->SetFlag(CPU::Flag::F_N, true);
    cpu->SetFlag(CPU::Flag::F_Z, true);
    op.Execute00(cpu, 0x18, CPU::Register8::B);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::B),(uint8_t) 0b11010101);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
    //Zero
    cpu->Set8BitRegister(CPU::Register8::B, 0b00000001);
    cpu->SetFlag(CPU::Flag::F_C, false);
    cpu->SetFlag(CPU::Flag::F_H, true);
    cpu->SetFlag(CPU::Flag::F_N, true);
    cpu->SetFlag(CPU::Flag::F_Z, true);
    op.Execute00(cpu, 0x18, CPU::Register8::B);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::B),(uint8_t) 0x0);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
    //carry
    cpu->Set8BitRegister(CPU::Register8::B, 0b01010101);
    cpu->SetFlag(CPU::Flag::F_C, false);
    cpu->SetFlag(CPU::Flag::F_H, true);
    cpu->SetFlag(CPU::Flag::F_N, true);
    cpu->SetFlag(CPU::Flag::F_Z, true);
    op.Execute00(cpu, 0x18, CPU::Register8::B);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::B),(uint8_t) 0b00101010);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
}
void GBEMUTest::testBMRL()
{
    initTest();

    BitManipOP op;
    CPU* cpu = new CPU(NULL);
    //Basic
    cpu->Set8BitRegister(CPU::Register8::B, 0b10101010);
    cpu->SetFlag(CPU::Flag::F_C, true);
    cpu->SetFlag(CPU::Flag::F_H, true);
    cpu->SetFlag(CPU::Flag::F_N, true);
    cpu->SetFlag(CPU::Flag::F_Z, true);
    op.Execute00(cpu, 0x10, CPU::Register8::B);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::B),(uint8_t) 0b01010101);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
    //Zero
    cpu->Set8BitRegister(CPU::Register8::B, 0b10000000);
    cpu->SetFlag(CPU::Flag::F_C, false);
    cpu->SetFlag(CPU::Flag::F_H, true);
    cpu->SetFlag(CPU::Flag::F_N, true);
    cpu->SetFlag(CPU::Flag::F_Z, true);
    op.Execute00(cpu, 0x10, CPU::Register8::B);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::B),(uint8_t) 0x0);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), true);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
    //carry
    cpu->Set8BitRegister(CPU::Register8::B, 0b01010101);
    cpu->SetFlag(CPU::Flag::F_C, false);
    cpu->SetFlag(CPU::Flag::F_H, true);
    cpu->SetFlag(CPU::Flag::F_N, true);
    cpu->SetFlag(CPU::Flag::F_Z, true);
    op.Execute00(cpu, 0x10, CPU::Register8::B);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::B),(uint8_t) 0b10101010);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);
}

void GBEMUTest::testSwap()
{
    initTest();
    BitManipOP op;
    CPU* cpu = new CPU(NULL);
    //Basic
    cpu->Set8BitRegister(CPU::Register8::B, 0b11110000);
    cpu->SetFlag(CPU::Flag::F_C, true);
    cpu->SetFlag(CPU::Flag::F_H, true);
    cpu->SetFlag(CPU::Flag::F_N, true);
    cpu->SetFlag(CPU::Flag::F_Z, true);
    op.Swap(cpu,  CPU::Register8::B);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::B),(uint8_t) 0b00001111);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::F),(uint8_t) 0b0);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_C), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_Z), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_H), false);
    QCOMPARE(cpu->GetFlag(CPU::Flag::F_N), false);


}

void GBEMUTest::testSLA()
{
    initTest();
    BitManipOP op;
    CPU* cpu = new CPU(NULL);
    cpu->Set8BitRegister(CPU::Register8::B, 0b11110000);
    cpu->SetFlag(CPU::Flag::F_C, true);
    cpu->SetFlag(CPU::Flag::F_H, true);
    cpu->SetFlag(CPU::Flag::F_N, true);
    cpu->SetFlag(CPU::Flag::F_Z, true);
    op.ShiftArithmetic(cpu, true, CPU::Register8::B);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::B),(uint8_t) 0b11111000);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::F), 0b0);
    cpu->Set8BitRegister(CPU::Register8::B, 0b11110000);
    cpu->SetFlag(CPU::Flag::F_C, true);
    cpu->SetFlag(CPU::Flag::F_H, true);
    cpu->SetFlag(CPU::Flag::F_N, true);
    cpu->SetFlag(CPU::Flag::F_Z, true);
    op.ShiftArithmetic(cpu, false, CPU::Register8::B);
    QCOMPARE(cpu->Get8BitRegister(CPU::Register8::B),(uint8_t) 0b11100000);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::F), 0b00010000);
}

void GBEMUTest::testZBIT()
{
    initTest();
    BitManipOP op;
    CPU* cpu = new CPU(NULL);
    //Test 0
    cpu->Set8BitRegister(CPU::Register8::B, 0b11101111);
    cpu->SetFlag(CPU::Flag::F_C, true);
    cpu->SetFlag(CPU::Flag::F_H, true);
    cpu->SetFlag(CPU::Flag::F_N, true);
    cpu->SetFlag(CPU::Flag::F_Z, true);

    op.ZBit(cpu,5, CPU::Register8::B);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::F),0b00110000);
}
void GBEMUTest::testOffsetOne()
{
    initTest();
    OffsetOne op;
    CPU* cpu = new CPU(NULL);
    cpu->Set8BitRegister(CPU::Register8::B, 0b00000001);
    OPCodeParameters p(cpu);
    p.OPCode = 0x4;
    op.Execute(p);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::B),0b10);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::F),0);
    p.OPCode = 0x5;
    op.Execute(p);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::B),0b1);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::F),0b01000000);
    op.Execute(p);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::B),0b0);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::F),0b11000000);
    op.Execute(p);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::B),0b11111111);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::F),0b01100000);
    p.OPCode=0x4;
    op.Execute(p);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::B),0b0);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::F),0b10100000);
}
void GBEMUTest::testPushPop()
{
    initTest();
    POP op;
    PUSH op1;
    Bus* bus = new Bus();
    bus->Map = new MemoryMapMock(bus);
    bus->Cpu = new CPU(bus);
    CPU* cpu = bus->Cpu;
    cpu->Set8BitRegister(CPU::Register8::B, 0xff);
    cpu->Set8BitRegister(CPU::Register8::C, 0x0);
    cpu->Set16BitRegister(CPU::Register16::SP,0xFFFE);
    OPCodeParameters p(cpu);
    p.OPCode = 0xC5;
    op1.Execute(p);
    QCOMPARE((int)cpu->Get16bitRegisterValue(CPU::Register16::SP), 0xFFFC);
    cpu->Set8BitRegister(CPU::Register8::B, 0x0);
    cpu->Set8BitRegister(CPU::Register8::C, 0xff);
    p.OPCode = 0xC5;
    op1.Execute(p);
    QCOMPARE((int)cpu->Get16bitRegisterValue(CPU::Register16::SP), 0xFFFA);
    p.OPCode = 0xc1;
    op.Execute(p);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::B),0x0);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::C),0xFF);
    QCOMPARE((int)cpu->Get16bitRegisterValue(CPU::Register16::SP), 0xFFFC);
    p.OPCode = 0xc1;
    op.Execute(p);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::B),0xFF);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::C),0x0);
    QCOMPARE((int)cpu->Get16bitRegisterValue(CPU::Register16::SP), 0xFFFE);
}

void GBEMUTest::testMOV()
{
    initTest();
    MOV op;
    CPU* cpu = new CPU(NULL);
    cpu->Set8BitRegister(CPU::Register8::B, 0b10101010);
    OPCodeParameters p(cpu);
    p.OPCode = 0x50;
    op.Execute(p);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::B),(int)cpu->Get8BitRegister(CPU::Register8::D));
    p.OPCode = 0x4A;
    op.Execute(p);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::C),(int)cpu->Get8BitRegister(CPU::Register8::B));
}
void GBEMUTest::testXOR()
{
    initTest();
    LOGIC op;
    CPU* cpu = new CPU(NULL);
    cpu->Set16BitRegister(CPU::Register16::AF,0b0101010111110000);
    cpu->Set8BitRegister(CPU::Register8::B, 0b10101010);
    OPCodeParameters p(cpu);
    p.OPCode=0xA8;
    op.Execute(p);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::F),0b0);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::A),0xFF);
    cpu->Set8BitRegister(CPU::Register8::B, 0xFF);
    op.Execute(p);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::A),0x0);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::F),0b10000000);

}
void GBEMUTest::testAND()
{
    initTest();
    LOGIC op;
    CPU* cpu = new CPU(NULL);
    cpu->Set16BitRegister(CPU::Register16::AF,0b0101010111110000);
    cpu->Set8BitRegister(CPU::Register8::B, 0b10101010);
    OPCodeParameters p(cpu);
    p.OPCode=0xA0;
    op.Execute(p);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::F),0b10100000);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::A),0x0);
    cpu->Set8BitRegister(CPU::Register8::B, 0xFF);
    cpu->Set8BitRegister(CPU::Register8::A, 0xFF);
    op.Execute(p);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::A),0xFF);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::F),0b00100000);
}
void GBEMUTest::testJPHL()
{
    initTest();
    Bus* bus = new Bus();
    bus->Map = new MemoryMapMock(bus);
    bus->Cpu = new CPU(bus);
    JPHL op;
    CPU* cpu = bus->Cpu;
    cpu->Set16BitRegister(CPU::Register16::AF,0b0101010111110000);
    cpu->Set16BitRegister(CPU::Register16::HL, 0x10FF);
    OPCodeParameters p(cpu);
    op.Execute(p);
    QCOMPARE((int)cpu->Get16bitRegisterValue(CPU::Register16::PC), 0x10FF);
}
void GBEMUTest::testLDHLSPI()
{
    initTest();
    Bus* bus = new Bus();
    bus->Map = new MemoryMapMock(bus);
    bus->Cpu = new CPU(bus);
    LDHLSPI op;
    CPU* cpu = bus->Cpu;
    cpu->Set16BitRegister(CPU::Register16::AF,0b0101010111110000);
    cpu->Set16BitRegister(CPU::Register16::SP, 0x1000);
    OPCodeParameters p(cpu);
    p.LowByte = 0xF1;//Signed -15
    op.Execute(p);
    QCOMPARE((int)cpu->Get16bitRegisterValue(CPU::Register16::HL), 0x0FF1);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::F),0b00100000);
    cpu->Set16BitRegister(CPU::Register16::SP, 0x000E);
    op.Execute(p);
    QCOMPARE((int)cpu->Get16bitRegisterValue(CPU::Register16::HL), 0xFFFF);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::F),0b00110000);
    cpu->Set16BitRegister(CPU::Register16::SP, 0x1000);
    p.LowByte = 0x10;
    op.Execute(p);
    QCOMPARE((int)cpu->Get16bitRegisterValue(CPU::Register16::HL), 0x1010);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::F),0b00000000);
    cpu->Set16BitRegister(CPU::Register16::SP, 0xFFFF);
    p.LowByte = 0x1;
    op.Execute(p);
    QCOMPARE((int)cpu->Get16bitRegisterValue(CPU::Register16::HL), 0x0);
    QCOMPARE((int)cpu->Get8BitRegister(CPU::Register8::F),0b00110000);
}
