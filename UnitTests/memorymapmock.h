#ifndef MEMORYMAPMOCK_H
#define MEMORYMAPMOCK_H
#include "../Emulator/includes.h"

class MemoryMapMock : public MemoryMap
{
public:
    MemoryMapMock(Bus* val);
    void SetMemory(uint16_t address, uint8_t data);
    uint8_t GetMemory(uint16_t address);

};

#endif // MEMORYMAPMOCK_H
