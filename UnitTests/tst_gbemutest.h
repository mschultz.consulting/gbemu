
#ifndef TSTS
#define TSTS
#include "../Emulator/includes.h"

class GBEMUTest 
{
private:
    int testNumber;
    void initTest();
public:
    GBEMUTest();
    ~GBEMUTest(){return;}
    void testAddSub_BasicAdd();
    void testAddSub_AddWithCarryZero();
    void testAddSub_BasicSubtract();
    void testAddSub_SubtractWithCarryZero();
    void testAddSub_ADC();
    void testAddSub_SBC();
    void testGPU_LCDC();
    void testGPU_BGP();
    void testGPU_OBP0();
    void testGPU_OBP1();
    void testPixelDotData();
    void testCFOp_Set();
    void testCFOp_Comp();
    void testBMZBit();
    void testBMRES();
    void testCP();
    void testBMRRC();
    void testBMRL();
    void testBMRR();
    void TestCALL();
    void testSwap();
    void testSLA();
    void testZBIT();
    void testOffsetOne();
    void testPushPop();
    void testMOV();
    void testXOR();
    void testAND();
    void testJPHL();
    void testLDHLSPI();
    void ExecuteTest()
    {
        testNumber = 0;
        testAddSub_BasicAdd();
        testAddSub_AddWithCarryZero();
        testAddSub_BasicSubtract();
        testAddSub_SubtractWithCarryZero();
        testAddSub_ADC();
        testAddSub_SBC();
        testGPU_LCDC();
        testGPU_BGP();
        testGPU_OBP0();
        testGPU_OBP1();
        testPixelDotData();
        testCFOp_Set();
        testCFOp_Comp();
        testBMZBit();
        testBMRES();
        testCP();
        testBMRRC();
        testBMRL();
        testBMRR();
        TestCALL();
        testSwap();
        testSLA();
        testZBIT();
        testOffsetOne();
        testPushPop();
        testMOV();
        testXOR();
        testAND();
        testJPHL();
        testLDHLSPI();
    }
};



#endif
