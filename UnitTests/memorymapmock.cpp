#include "memorymapmock.h"

MemoryMapMock::MemoryMapMock(Bus* val) : MemoryMap::MemoryMap(val, false)
{
    MemoryMap::SecondInternalRam = new uint8_t[0xFF];
}

void MemoryMapMock::SetMemory(uint16_t address, uint8_t data)
{
    if (address > SECOND_EMPTY_MAX_ADDR )
        MemoryMap::SetMemory(address, data);
}
uint8_t MemoryMapMock::GetMemory(uint16_t address)
{
    if (address > SECOND_EMPTY_MAX_ADDR)
        return MemoryMap::GetMemory(address);
    return 0xFF;
}
